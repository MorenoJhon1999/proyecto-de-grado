<?php
	session_start();

	if(!isset($_SESSION['rol'])){
		header('Location: index.php');
	}else{

		if($_SESSION['rol'] != 2){
			header('Location: index.php');
		}
	}

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sala Estudiantes</title>
    <script src="https://kit.fontawesome.com/9e00248cd3.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="./css/normalize.css">
	<link rel="stylesheet" href="./css/bootstrap.min.css">
	<link rel="stylesheet" href="./css/bootstrap-material-design.min.css">
	<link rel="stylesheet" href="./css/all.css">
	<link rel="stylesheet" href="./css/sweetalert2.min.css">
	<script src="./js/sweetalert2.min.js" ></script>
	<link rel="stylesheet" href="./css/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" href="./css/style.css">
</head>
<body>

<!-- Main container -->
<main class="full-box main-container">
		<!-- Nav lateral -->

		<?php
		$id_estado = $_GET['estadoid'];
		if ($id_estado == 1)
			include "nav-lateral-estudiantes.php";
		elseif ($id_estado == 2)
			include "nav-estudiante-lateral-proyecto.php";
		?>

		<?php
		$id_estado = $_GET['estadoid'];
		if($id_estado == 1){
			include "section-estudiante-anteproyecto.php";
		}
		elseif ($id_estado == 2){
			include "section-estudiante-proyecto.php";
		}
		
		?>
	

		</section>
	</main>
<script src="./js/jquery-3.4.1.min.js" ></script>
	<script src="./js/popper.min.js" ></script>
	<script src="./js/bootstrap.min.js" ></script>
	<script src="./js/jquery.mCustomScrollbar.concat.min.js" ></script>
	<script src="./js/bootstrap-material-design.min.js" ></script>
	<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
	<script src="./js/main.js" ></script>
</body>
</html>