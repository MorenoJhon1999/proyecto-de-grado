<?php
	session_start();

	if(!isset($_SESSION['rol'])){
		header('Location: index.php');
	}else{

		if($_SESSION['rol'] != 1){
			header('Location: index.php');
		}
	}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reportede uso</title>
    <script src="https://kit.fontawesome.com/9e00248cd3.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="./css/normalize.css">
	<link rel="stylesheet" href="./css/bootstrap.min.css">
	<link rel="stylesheet" href="./css/bootstrap-material-design.min.css">
	<link rel="stylesheet" href="./css/all.css">
	<link rel="stylesheet" href="./css/sweetalert2.min.css">
	<script src="./js/sweetalert2.min.js" ></script>
	<link rel="stylesheet" href="./css/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" href="./css/style.css">
</head>
<body>
    <!-- Main container -->
	<main class="full-box main-container">
	<?php
		$usertype = $_GET['usertype'];
		if ($usertype == 3)
			include "nav-lateral-asesor.php";
		elseif ($usertype == 2)
			include "nav-lateral-estudiantes.php";
		elseif ($usertype == 1)
			include "nav-lateral-coordinador.php";
			?>

		<!-- Page content -->
		<section class="full-box page-content">
			<nav class="full-box navbar-info">
				<a href="#" class="float-left show-nav-lateral">
					<i class="fas fa-exchange-alt"></i>
				</a>
				<a href="user-update.html">
					<i class="fas fa-user-cog"></i>
				</a>
				<a href="http://localhost/Gestion-de-Anteproyectos-and-Proyectos-de-Grado%20Uniminuto/index.php?cerrar-session=1" class="btn-exit-system">
					<i class="fas fa-power-off"></i>
				</a>
			</nav>

			<!-- Page header -->
			<div class="full-box page-header">
				<h3 class="text-center"><strong>Reporte general de uso de la aplicación</strong></h3>
			</div>


			<!-- Content here-->
			<div class="container-fluid">
				<div class="table-responsive">
					<table class="table table-dark table-sm">
						<thead>
							<tr class="text-center roboto-medium">
								<th>TOTAL IDEAS REGISTRADAS</th>
								<th>TOTAL OBSERVACIONES</th>
								<th>TOTAL RECHAZADAS</th>
								<th>TOTAL APROBADAS</th>
								<th>TOTAL EN REVISIÓN</th>
								<th>PROMEDIO DE OBSERVACIONES POR IDEA</th>
							</tr>
						</thead>
						<?php
						include 'read-reporte.php';
						?>
					</table>
				</div>				
			</div>
		</section>
	</main>
    <script src="./js/jquery-3.4.1.min.js" ></script>
	<script src="./js/popper.min.js" ></script>
	<script src="./js/bootstrap.min.js" ></script>
	<script src="./js/jquery.mCustomScrollbar.concat.min.js" ></script>
	<script src="./js/bootstrap-material-design.min.js" ></script>
	<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
	<script src="./js/main.js" ></script>
</body>
</html>