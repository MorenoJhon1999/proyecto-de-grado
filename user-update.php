<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Actulizar Datos</title>
    <script src="https://kit.fontawesome.com/9e00248cd3.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="./css/normalize.css">
	<link rel="stylesheet" href="./css/bootstrap.min.css">
	<link rel="stylesheet" href="./css/bootstrap-material-design.min.css">
	<link rel="stylesheet" href="./css/all.css">
	<link rel="stylesheet" href="./css/sweetalert2.min.css">
	<link rel="stylesheet" href="./css/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" href="./css/style.css">
</head>
<body>

<main class="full-box main-container">
		<!-- Nav lateral -->
		<?php
		$usertype = $_GET['usertype'];
		$idea_estado = $_GET['estadoid'];

		if ($usertype == 3){
			include "nav-lateral-asesor.php";
		}
			
		else if ($usertype == 2 && $idea_estado == 1){
			include "nav-lateral-estudiantes.php";
		}
			
		else if ($usertype == 2 && $idea_estado == 2){
			include "nav-estudiante-lateral-proyecto.php";
		}
		
		elseif ($usertype == 1 && $idea_estado == 3){
			include "nav-lateral-coordinador.php";
		}
			
		?>
		<!-- Page content -->
		<section class="full-box page-content">
			<nav class="full-box navbar-info">
				<a href="#" class="float-left show-nav-lateral">
					<i class="fas fa-exchange-alt"></i>
				</a>
				<a href="user-update.html">
					<i class="fas fa-user-cog"></i>
				</a>
				<a href="#" class="btn-exit-system">
					<i class="fas fa-power-off"></i>
				</a>
			</nav>

			<!-- Page header -->
			<div class="full-box page-header">
				<h3 class="text-left">
					<i class="fas fa-sync-alt fa-fw"></i> &nbsp; ACTUALIZAR USUARIO
				</h3>
				<p class="text-justify">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit nostrum rerum animi natus beatae ex. Culpa blanditiis tempore amet alias placeat, obcaecati quaerat ullam, sunt est, odio aut veniam ratione.
				</p>
			</div>
			
			<div class="container-fluid">
			<?php include ('actualizar_contraseña.php'); ?>
				<form  method="POST" class="form-neon"  autocomplete="off" enctype="multipart/form-data">
					<fieldset>
						<legend><i class="fas fa-sync-alt"></i> &nbsp; ACTUALIZACION DE CONTRASEÑA</legend>			
						<div class="container-fluid">
							<div class="row">

								<div class="col-12 col-md-6">
									<div class="form-group">
										<label for="contraseña1" class="bmd-label-floating">Contraseña Nueva</label>
										<input type="password" class="form-control" name="contraseña1" id="contraseña1"  maxlength="10" required>
									</div>
								</div>

								<div class="col-12 col-md-6">
									<div class="form-group">
										<label for="contraseña2" class="bmd-label-floating">Repetir Contraseña</label>
										<input type="password" class="form-control" name="contraseña2"   maxlength="40" required>
									</div>
								</div>

							</div>
						</div>
					</fieldset>
					<br><br><br>
					<p class="text-center" style="margin-top: 40px;">
					<button type="submit" name="Enviar" class="btn btn-raised btn-success btn-sm"><i class="fas fa-sync-alt"></i> &nbsp; ACTUALIZAR</button>
					</p>
				</form>
			</div>	


	
		</section>
	</main>
	<script src="./js/sweetalert2.min.js" ></script>
    <script src="./js/jquery-3.5.1.min.js"></script>
    <script src="./js/enviar-datos.js"></script>
    <script src="./js/jquery-3.4.1.min.js" ></script>
	<script src="./js/popper.min.js" ></script>
	<script src="./js/bootstrap.min.js" ></script>
	<script src="./js/jquery.mCustomScrollbar.concat.min.js" ></script>
	<script src="./js/bootstrap-material-design.min.js" ></script>
	<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
	<script src="./js/main.js" ></script>
</body>
</html>

