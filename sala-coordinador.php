<?php
	session_start();

	if(!isset($_SESSION['rol'])){
		header('Location: index.php');
	}else{

		if($_SESSION['rol'] != 1){
			header('Location: index.php');
		}
	}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sala Coordinador</title>
    <script src="https://kit.fontawesome.com/9e00248cd3.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="./css/normalize.css">
	<link rel="stylesheet" href="./css/bootstrap.min.css">
	<link rel="stylesheet" href="./css/bootstrap-material-design.min.css">
	<link rel="stylesheet" href="./css/all.css">
	<link rel="stylesheet" href="./css/sweetalert2.min.css">
	<script src="./js/sweetalert2.min.js" ></script>
	<link rel="stylesheet" href="./css/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" href="./css/style.css">
</head>
<body>

<!-- Main container -->
<main class="full-box main-container">
		<!-- Nav lateral -->
		<?php include ('nav-lateral-coordinador.php') ?>

		<!-- Page content -->
		<section class="full-box page-content">
			<nav class="full-box navbar-info">
				<a href="#" class="float-left show-nav-lateral">
					<i class="fas fa-exchange-alt"></i>
				</a>
				<a href="user-update.php?usertype=1&userid=<?=$userid;?>&estadoid=<?=$idea_estado;?>">
					<i class="fas fa-user-cog"></i>
				</a>
				<a href="http://localhost/Gestion-de-Anteproyectos-and-Proyectos-de-Grado%20Uniminuto/index.php?cerrar-session=1" class="btn-exit-system">
					<i class="fas fa-power-off"></i>
				</a>
			</nav>

			

			<!-- Page header -->
			<div class="full-box page-header">
				<h3 class="text-left">
					<i class="fab fa-dashcube fa-fw"></i> &nbsp; DASHBOARD
				</h3>
				<p class="text-justify">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit nostrum rerum animi natus beatae ex. Culpa blanditiis tempore amet alias placeat, obcaecati quaerat ullam, sunt est, odio aut veniam ratione.
				</p>
			</div>
			
			<!-- Content -->
			<div class="full-box tile-container">

					<?php
						include_once "conexion.php";
						$sql_estudiantes = "SELECT count(id) AS cantidad_usuarios
						FROM usuarios ";
						$datos_estudiantes = mysqli_query ($conexion,$sql_estudiantes);
						$row_estudiantes = mysqli_fetch_array($datos_estudiantes);
					?>
				
				<a href="registro-usuarios.php?registro-usuarios=3&userid=<?=$userid;?>" class="tile">
					<div class="tile-tittle">Registro de Usuarios</div>
					<div class="tile-icon">
						<img src="assets/avatares/community_users_12977.png" width="150" height="120" alt="">
						<p> <?php echo $row_estudiantes['cantidad_usuarios'] ?> Registrados</p>
					</div>
				</a>

				<a href="registro-proyecto.php?registro-usuarios=3&userid=<?=$userid;?>&estadoid=<?=$idea_estado;?>" class="tile">
					<div class="tile-tittle">Registro de Proyectos</div>
					<div class="tile-icon">
						<img src="assets/avatares/pruebas.png" width="150" height="120" alt="">
						<p> <?php echo $row_estudiantes['cantidad_usuarios'] ?> Registrados</p>
					</div>
				</a>

				<?php
						include_once "conexion.php";
						$sql_asesor = "SELECT count(id) AS cantidad_asesor
						FROM usuarios WHERE  id_rol = 3 ";
						$datos_asesor = mysqli_query ($conexion,$sql_asesor);
						$row_estudiantes = mysqli_fetch_array($datos_asesor);
					?>

				<a href="asignar-asesor.php?asignacion-asesor=3&userid=<?=$userid;?>" class="tile">
					<div class="tile-tittle">Asignacion de Asesor A</div>
					<div class="tile-icon">
                    <img src="assets/avatares/recurso.png" width="150" height="120" alt="">
						<p><?php echo $row_estudiantes['cantidad_asesor'] ?> Asesores Resgitrados</p>
					</div>
				</a>


				<a href="asignar-asesor-proyecto.php?asignacion-asesor=3&userid=<?=$userid;?>" class="tile">
					<div class="tile-tittle">Asignacion de Asesor P</div>
					<div class="tile-icon">
                    <img src="assets/avatares/rotacion.png" width="150" height="120" alt="">
						<p><?php echo $row_estudiantes['cantidad_asesor'] ?> Asesores Resgitrados</p>
					</div>
				</a>


				
				<a href="horas_asesor.php?asignacion-asesor=3&userid=<?=$userid;?>" class="tile">
					<div class="tile-tittle">Asignar horas de Asesor</div>
					<div class="tile-icon">
                    <img src="assets/avatares/reloj.png" width="150" height="120" alt="">
						<p><?php echo $row_estudiantes['cantidad_asesor'] ?> Asesores Resgitrados</p>
					</div>
				</a>


	
				<a href="asignar-horas-jurado.php?asignacion-asesor=3&userid=<?=$userid;?>" class="tile">
					<div class="tile-tittle">Asignar horas de Jurado</div>
					<div class="tile-icon">
                    <img src="assets/avatares/calendario.png" width="150" height="120" alt="">
						<p><?php echo $row_estudiantes['cantidad_asesor'] ?> Asesores Resgitrados</p>
					</div>
				</a>



				<a href="asiganacion-jurado-proyecto.php?asignacion-asesor=3&userid=<?=$userid;?>" class="tile">
					<div class="tile-tittle">Asignacion de Jurado</div>
					<div class="tile-icon">
                    <img src="assets/avatares/jurado.png" width="150" height="120" alt="">
						<p><?php echo $row_estudiantes['cantidad_asesor'] ?> Asesores Resgitrados</p>
					</div>
				</a>

				<?php
						include_once "conexion.php";
						$sql_ideas = "SELECT count(id_proyecto) AS cantidad_proyecto
						FROM ideas ";
						$datos_ideas = mysqli_query ($conexion,$sql_ideas);
						$row_estudiantes = mysqli_fetch_array($datos_ideas);
					?>

				<a href="consulta-ideas.php?usertype=1&userid=<?=$userid;?>" class="tile">
					<div class="tile-tittle">Consulta de Ideas</div>
					<div class="tile-icon">
                    <img src="assets/avatares/investigacion.png" width="150" height="120" alt="">
						<p><?php echo $row_estudiantes['cantidad_proyecto'] ?> Ideas Registradas</p>
					</div>
				</a>

				<a href="consulta-proyectos.php?usertype=1&userid=<?=$userid;?>" class="tile">
					<div class="tile-tittle">Consulta de Proyectos</div>
					<div class="tile-icon">
                    <img src="assets/avatares/consulta-de-busqueda.png" width="150" height="120" alt="">
						<p><?php echo $row_estudiantes['cantidad_proyecto'] ?> Ideas Registradas</p>
					</div>
				</a>

				<a href="consulta-horas-asesoria-jurado.php?usertype=1&userid=<?=$userid;?>" class="tile">
					<div class="tile-tittle">Consulta horas asignadas</div>
					<div class="tile-icon">
                    <img src="assets/avatares/consultante.png" width="150" height="120" alt="">
						<p><?php echo $row_estudiantes['cantidad_proyecto'] ?> Ideas Registradas</p>
					</div>
				</a>


				<a target="_blank" href="inicio.php" class="tile">
					<div class="tile-tittle">C.Redactar Anteproyecto</div>
					<div class="tile-icon">
					<img src="assets/avatares/note-task-comment-message-edit-write_108613.png" width="150" height="120" alt="">
						<p>UNIMINUTO</p>
					</div>
                </a>
                
                <a href="reporte-de-uso.php?usertype=1&userid=<?=$userid;?>" class="tile">
					<div class="tile-tittle">Resportes de Uso</div>
					<div class="tile-icon">
                    <img src="assets/avatares/reporte.png" width="150" height="120" alt="">
						<p>1 Registrada</p>
					</div>
				</a>

				<a href="consulta-asesor.php?usertype=1&userid=<?=$userid;?>" class="tile">
					<div class="tile-tittle">Consulta Asesores</div>
					<div class="tile-icon">
                    <img src="assets/avatares/profesores.png" width="150" height="120" alt="">
						<p>1 Registrada</p>
					</div>
				</a>

				<a href="consulta-estudiantes.php?usertype=1&userid=<?=$userid;?>" class="tile">
					<div class="tile-tittle">Consulta Estudiantes</div>
					<div class="tile-icon">
                    <img src="assets/avatares/buscar.png" width="150" height="120" alt="">
						<p>1 Registrada</p>
					</div>
				</a>

			</div>

			<div class="overlay" id="overlay">
			<div class="popup" id="popup">
				<a href="#" id="btn-cerrar-popup" class="btn-cerrar-popup"><i class="fas fa-times"></i></a>
				<h3><?=$nombre;?> <?=$apellido;?></h3>
				<h4>Selecciona la imagen que deseas cambiar.</h4>
				<div class="container-fluid">
            <?php include("insertar-imagen.php");?>
				<form  method="POST" class="form-neon" autocomplete="off" enctype="multipart/form-data">
					<fieldset>
						<div class="container-fluid">
                        <div class="row">
						<div class="col-12 col-md-12">
							<label for="documento" class="bmd-label-floating">Seleccione una imagen por favor</label>
							<input type="file" class="form-control" name="documento" id="documento" required></textarea>
                        </div>
                       
					</fieldset>
					<br><br><br>
					<p class="text-center" style="margin-top: 40px;">
						&nbsp; &nbsp;
						<button type="submit" name="Enviar" class="btn btn-raised btn-info btn-sm"><i class="far fa-save"></i> &nbsp; GUARDAR</button>
					</p>
				</form>
			</div>	
			</div>
		</div>
	</div>

		</section>
	</main>
	<script src="./js/jquery-3.4.1.min.js" ></script>
	<script src="./js/popper.min.js" ></script>
	<script src="./js/bootstrap.min.js" ></script>
	<script src="./js/imagenes.js"></script>
	<script src="./js/jquery.mCustomScrollbar.concat.min.js" ></script>
	<script src="./js/bootstrap-material-design.min.js" ></script>
	<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
	<script src="./js/main.js" ></script>
	
	
</body>
</html>