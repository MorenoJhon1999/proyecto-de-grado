
<?php
	session_start();

	if(!isset($_SESSION['rol'])){
		header('Location: index.php');
	}else{

		if($_SESSION['rol'] != 3){
			header('Location: index.php');
		}
	}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sala Asesor</title>
	<script src="https://kit.fontawesome.com/9e00248cd3.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="./css/normalize.css">
	<link rel="stylesheet" href="./css/bootstrap.min.css">
	<link rel="stylesheet" href="./css/bootstrap-material-design.min.css">
	<link rel="stylesheet" href="./css/all.css">
	<link rel="stylesheet" href="./css/sweetalert2.min.css">
	<script src="./js/sweetalert2.min.js" ></script>
	<link rel="stylesheet" href="./css/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" href="./css/style.css">
</head>
<body>
    	<!-- Main container -->
	<main class="full-box main-container">
		<!-- Nav lateral -->
		<?php include ('nav-lateral-asesor.php') ?>

		<!-- Page content -->
		<section class="full-box page-content">
			<nav class="full-box navbar-info">
				<a href="#" class="float-left show-nav-lateral">
				<i class="fas fa-exchange-alt"></i>
				</a>
				<a href="user-update.php?usertype=3&userid=<?=$userid;?>&estadoid=<?=$idea_estado;?>">
					<i class="fas fa-user-cog"></i>
				</a>
				<a href="#" class="btn-exit-system">
					<i class="fas fa-power-off"></i>
				</a>
			</nav>

			<!-- Page header -->
			<div class="full-box page-header">
				<h3 class="text-left">
					<i class="fab fa-dashcube fa-fw"></i> &nbsp; DASHBOARD
				</h3>
				<p class="text-justify">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit nostrum rerum animi natus beatae ex. Culpa blanditiis tempore amet alias placeat, obcaecati quaerat ullam, sunt est, odio aut veniam ratione.
				</p>
			</div>
			
			<!-- Content -->
			<div class="full-box tile-container">

				<a href="retroalimentacion-ideas.php?usertype=3&userid=<?=$userid;?>" class="tile">
					<div class="tile-tittle">Retroalimentacion</div>
					<div class="tile-icon">
						<img src="assets/avatares/satisfaccion.png" width="150" height="120" alt="">
						<p>5 Registrados</p>
					</div>
				</a>


				<a href="consulta-ideas.php?usertype=3&userid=<?=$userid;?>" class="tile">
					<div class="tile-tittle">Consulta de Ideas</div>
					<div class="tile-icon">
                    <img src="assets/avatares/investigacion.png" width="150" height="120" alt="">
						<p>50 Registrados</p>
					</div>
				</a>

				<a target="_blank" href="inicio.php?usertype=3&userid=<?=$userid;?>" class="tile">
					<div class="tile-tittle">C.Redactar Anteproyecto</div>
					<div class="tile-icon">
					<img src="assets/avatares/note-task-comment-message-edit-write_108613.png" width="150" height="120" alt="">
						<p>1 Registrada</p>
					</div>
                </a>
                
				<a href="consulta-asignacion.php?usertype=3&userid=<?=$userid;?>" class="tile">
					<div class="tile-tittle">Consulta de Asignacion</div>
					<div class="tile-icon">
                    <img src="assets/avatares/buscar.png" width="150" height="120" alt="">
						<p>1 Registrada</p>
					</div>
				</a>

				<a href="reporte_retroalimentacion.php?usertype=3&userid=<?=$userid;?>" class="tile">
					<div class="tile-tittle">Reporte Retroalimentacion</div>
					<div class="tile-icon">
                    <img src="assets/avatares/reporte1.png" width="150" height="120" alt="">
						<p>1 Registrada</p>
					</div>
				</a>
				
			</div>
			
			<div class="overlay" id="overlay">
			<div class="popup" id="popup">
				<a href="#" id="btn-cerrar-popup" class="btn-cerrar-popup"><i class="fas fa-times"></i></a>
				<h3><?=$nombre;?> <?=$apellido;?></h3>
				<h4>Selecciona la imagen que deseas cambiar.</h4>
				<div class="container-fluid">
            <?php include("insertar-imagen.php");?>
				<form  method="POST" class="form-neon" autocomplete="off" enctype="multipart/form-data">
					<fieldset>
						<div class="container-fluid">
                        <div class="row">
						<div class="col-12 col-md-12">
							<label for="documento" class="bmd-label-floating">Seleccione una imagen por favor</label>
							<input type="file" class="form-control" name="documento" id="documento" required></textarea>
                        </div>
                       
					</fieldset>
					<br><br><br>
					<p class="text-center" style="margin-top: 40px;">
						&nbsp; &nbsp;
						<button type="submit" name="Enviar" class="btn btn-raised btn-info btn-sm"><i class="far fa-save"></i> &nbsp; GUARDAR</button>
					</p>
				</form>
			</div>	
			</div>
		</div>
	</div>



		</section>
	</main>
    <script src="./js/jquery-3.4.1.min.js" ></script>
	<script src="./js/popper.min.js" ></script>
	<script src="./js/imagenes.js"></script>
	<script src="./js/bootstrap.min.js" ></script>
	<script src="./js/jquery.mCustomScrollbar.concat.min.js" ></script>
	<script src="./js/bootstrap-material-design.min.js" ></script>
	<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
	<script src="./js/main.js" ></script>
</body>
</html>