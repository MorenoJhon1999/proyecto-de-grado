
<form  method="post" enctype="multipart/form-data">
<fieldset>
<legend><i class="fas fa-user"></i> &nbsp; Retroalimentaciones</legend>
<div class="container-fluid">
     <div class="row">

		<div class="col-12 col-md-6">
			<div class="form-group">
			<label for="titulo" class="bmd-label-floating"> Titilo del Proyecto</label>
            <textarea type="text" class="form-control" name="titulo" id="titulo" disabled><?=$row["titulo"];?></textarea>
		</div>
		</div>

		<div class="col-12 col-md-6">
			<div class="form-group">
				<label for="palabras_clave" class="bmd-label-floating">Observaciones Titulo Poryecto</label>
		    	<textarea type="text" class="form-control" name="ob_titulo" id="ob_titulo" required></textarea>
		</div>
		</div>

		<div class="col-12 col-md-6">
			<div class="form-group">
			    <label for="planteamiento_problema" class="bmd-label-floating"> Palabras Claves</label>
                <textarea type="text" class="form-control" name="palabras_claves" id="palabras_claves" disabled><?=$row["palabras_claves"];?></textarea>
		</div>
		</div>

		<div class="col-12 col-md-6">
			<div class="form-group">
			<label for="objetivo_general" class="bmd-label-floating">Observaciones Palabras Claves</label>
			<textarea type="text" class="form-control" name="ob_palabras_claves" id="ob_palabras_claves" required></textarea>
		    </div>
		</div>

        <div class="col-12 col-md-6">
			<div class="form-group">
			<label for="planteamiento_problema" class="bmd-label-floating">Planteamiento del Problema</label>
            <textarea type="text" class="form-control" name="titulo" id="titulo" disabled><?=$row["planteamiento_problema"];?></textarea>
		</div>
		</div>

		<div class="col-12 col-md-6">
			<div class="form-group">
			<label for="objetivo_general" class="bmd-label-floating">Observaciones Planteamiento del Problema</label>
			<textarea type="text" class="form-control" name="ob_problema" id="ob_problema" required></textarea>
		    </div>
		</div>

        <div class="col-12 col-md-6">
			<div class="form-group">
			<label for="planteamiento_problema" class="bmd-label-floating">Obejetivo General</label>
            <textarea type="text" class="form-control" name="titulo" id="titulo" disabled><?=$row["objetivo_general"];?></textarea>
		</div>
		</div>

		<div class="col-12 col-md-6">
			<div class="form-group">
			<label for="objetivo_general" class="bmd-label-floating">Observaciones Obejetivo General</label>
			<textarea type="text" class="form-control" name="ob_general" id="ob_general" required></textarea>
		    </div>
		</div>

        <div class="col-12 col-md-6">
			<div class="form-group">
			<label for="planteamiento_problema" class="bmd-label-floating">Objetivos Especificos</label>
            <textarea type="text" class="form-control" name="titulo" id="titulo" disabled><?=$row["objetivo_especifico"];?></textarea>
		</div>
		</div>

		<div class="col-12 col-md-6">
			<div class="form-group">
			<label for="objetivo_general" class="bmd-label-floating">Observaciones Objetivos Especificos</label>
			<textarea type="text" class="form-control" name="ob_especifico" id="ob_especifico" required></textarea>
		    </div>
		</div>

        <div class="col-12 col-md-6">
			<div class="form-group">
			<label for="planteamiento_problema" class="bmd-label-floating">Justificacion</label>
            <textarea type="text" class="form-control" name="titulo" id="titulo" disabled><?=$row["justificacion"];?></textarea>
		</div>
		</div>

		<div class="col-12 col-md-6">
			<div class="form-group">
			<label for="objetivo_general" class="bmd-label-floating">Observaciones Justificacion</label>
			<textarea type="text" class="form-control" name="ob_justificacion" id="ob_justificacion" required></textarea>
		    </div>
		</div>


        <div class="col-12 col-md-6">
			<div class="form-group">
            <a class=" nav-item descargar"  target="_blank" href="documentos/<?=$row['documento']?>"><i class="fas fa-download"></i> Descargar documento</a>
		    </div>
        </div>

		<div class="col-12 col-md-6">
				<label for="documento" class="bmd-label-floating">Seleccione un documento de texto</label>
				<input type="file" class="form-control" name="documento" id="documento" required></textarea>
        </div>

		<div class="col-12 col-md-6">
			<div class="form-group">
			<label for="id_proyecto" class="bmd-label-floating">ID del Proyecto</label>
			<input type="number"  class="form-control" name="id_proyecto" value="<?php echo $id_idea ?>" readonly id="buscarOb" maxlength="6" required>
		    </div>
		</div>

        
        <div class="col-12 col-md-6">
			<div class="form-group">
			<label for="usr"><b>Estado:</b></label>
            <select class="form-control" required id="estado" name="estado">
            <option value="#"></option>
            <option value="Aceptado">Aceptado</option>
            <option value="Rechazado">Rechazado</option>
            <option value="revision">Revision</option>
            </select>
		</div>
		</div>


	    </fieldset>
		<br><br><br>
		<p class="text-center" style="margin-top: 40px;">
		<button type="reset" class="btn btn-raised btn-secondary btn-sm"><i class="fas fa-paint-roller"></i> &nbsp; LIMPIAR</button>
		
		<?php 

		
		$consulta = " SELECT id_retroalimentacion FROM retroalimentacion_idea
		WHERE id_proyecto = $id_idea";
		$resul = mysqli_query ($conexion,$consulta);
		$respuesta =  mysqli_fetch_assoc($resul);

		$resultado = $respuesta["id_retroalimentacion"];


		if($resultado <=0){
			echo '
			&nbsp; &nbsp;
			<button type="submit" name="Enviar" class="btn btn-raised btn-info btn-sm"><i class="far fa-save"></i> &nbsp; GUARDAR</button>';
		}
		else if ($resultado >= 1) {
			echo '&nbsp; &nbsp;
			<button type="submit" name="Actualizar" class="btn btn-raised btn-info btn-sm"><i class="far fa-save"></i> &nbsp; ACTUALIZAR RETROALIMENTACION</button>';
		}
		?>
		</p>
    </div>
</div>
</form>