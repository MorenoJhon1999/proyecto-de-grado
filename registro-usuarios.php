<?php
	session_start();

	if(!isset($_SESSION['rol'])){
		header('Location: index.php');
	}else{

		if($_SESSION['rol'] != 1){
			header('Location: index.php');
		}
	}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro Usuarios</title>
    <script src="https://kit.fontawesome.com/9e00248cd3.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="./css/normalize.css">
	<link rel="stylesheet" href="./css/bootstrap.min.css">
	<link rel="stylesheet" href="./css/bootstrap-material-design.min.css">
	<link rel="stylesheet" href="./css/all.css">
	<link rel="stylesheet" href="./css/sweetalert2.min.css">
	<link rel="stylesheet" href="./css/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" href="./css/style.css">
</head>
<body>
<!-- Main container -->
<main class="full-box main-container">
		<!-- Nav lateral -->
		<?php include ('nav-lateral-coordinador.php') ?>
		<!-- Page content -->
		<section class="full-box page-content">
			<nav class="full-box navbar-info">
				<a href="#" class="float-left show-nav-lateral">
					<i class="fas fa-exchange-alt"></i>
				</a>
				<a href="user-update.html">
					<i class="fas fa-user-cog"></i>
				</a>
				<a href="#" class="btn-exit-system">
					<i class="fas fa-power-off"></i>
				</a>
			</nav>

			<!-- Page header -->
			<div class="full-box page-header">
				<h3 class="text-left">
					<i class="fas fa-plus fa-fw"></i> &nbsp; AGREGAR USUARIO
				</h3>
				<p class="text-justify">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem odit amet asperiores quis minus, dolorem repellendus optio doloremque error a omnis soluta quae magnam dignissimos, ipsam, temporibus sequi, commodi accusantium!
				</p>
			</div>

		
			
			<!-- Content here-->
			<div class="container-fluid">
				<form id="frregistro" method="POST" class="form-neon"  autocomplete="off">
					<fieldset>
						<legend><i class="fas fa-user"></i> &nbsp; Información básica</legend>
						<div class="container-fluid">
							<div class="row">
								<div class="col-12 col-md-6">
									<div class="form-group">
										<label for="cliente_dni" class="bmd-label-floating text-dark">ID:</label>
										<input type="number" class="form-control" name="id_usuario" id="cliente_dni" maxlength="27" required>
									</div>
								</div>
								<div class="col-12 col-md-6">
									<div class="form-group">
										<label for="cliente_nombre" class="bmd-label-floating text-dark">Nombres:</label>
										<input type="text"  class="form-control" name="nombres" id="cliente_nombre" maxlength="40" required>
									</div>
								</div>
								<div class="col-12 col-md-6">
									<div class="form-group">
										<label for="cliente_apellido" class="bmd-label-floating text-dark">Apellidos:</label>
										<input type="text" class="form-control" name="apellidos" id="cliente_apellido" maxlength="40" required>
									</div>
                                </div>
                                <div class="col-12 col-md-6">
									<div class="form-group">
										<label for="cliente_telefono" class="bmd-label-floating text-dark">Correo:</label>
										<input type="email"  class="form-control" name="correo" id="cliente_telefono" maxlength="40" required>
									</div>
								</div>
								<div class="col-12 col-md-6">
									<div class="form-group">
										<label for="cliente_telefono" class="bmd-label-floating text-dark">Teléfono:</label>
										<input type="text" class="form-control" name="telefono" id="cliente_telefono" maxlength="20" required>
									</div>
								</div>

								<div class="col-12 col-md-6">
									<div class="form-group">
										<label for="cliente_telefono" class="bmd-label-floating text-dark">Contraseña Usuario</label>
										<input type="text" class="form-control" name="contraseña_user"  maxlength="20" title="Genere una contraseña al usuario" required>
									</div>
								</div>

								<div class="col-12 col-md-6">
									<div class="form-group">
									<label for="usr"><b class = "text-dark">Codigo de Usuario</b></label>
                                    <select class="form-control" required id="codigo_user" name="codigo_user">
									<option value="#"></option>
                                    <option value="1">Administrador</option>
                                    <option value="2">Estudiante</option>
                                    <option value="3">Asesor</option>
                                    </select>
									</div>
								</div>

								<div class="col-12 col-md-6">
									<div class="form-group">
									<label for="usr"><b class = "text-dark">Estado Idea</b></label>
                                    <select class="form-control" required id="codigo_idea" name="codigo_idea">
									<option value="#"></option>
                                    <option value="1">Anteproyecto</option>
                                    <option value="2">Proyecto de Grado</option>
                                    <option value="3">No Aplica</option>
                                    </select>
									</div>
								</div>


							</div>
						</div>
					</fieldset>
					<br><br><br>
					<p class="text-center" style="margin-top: 40px;">
						<button type="reset" class="btn btn-raised btn-secondary btn-sm"><i class="fas fa-paint-roller"></i> &nbsp; LIMPIAR</button>
						&nbsp; &nbsp;
						<button  id="registrar" class="btn btn-raised btn-info btn-sm"><i class="far fa-save"></i> &nbsp; GUARDAR</button>
					</p>
                </form>
			</div>
		</section>
	</main>
    <script src="./js/sweetalert2.min.js" ></script>
    <script src="./js/jquery-3.5.1.min.js"></script>
    <script src="./js/enviar-datos.js"></script>
    <script src="./js/jquery-3.4.1.min.js" ></script>
	<script src="./js/popper.min.js" ></script>
	<script src="./js/bootstrap.min.js" ></script>
	<script src="./js/jquery.mCustomScrollbar.concat.min.js" ></script>
	<script src="./js/bootstrap-material-design.min.js" ></script>
	<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
	<script src="./js/main.js" ></script>
</body>
</html>