<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Anteproyecto</title>
    <script src="https://kit.fontawesome.com/9e00248cd3.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/bootstrap-material-design.min.css">
    <link rel="stylesheet" href="./css/micrositio.css">
</head>
<body>
<nav id="inicio" class="navbar navbar-expand-lg navbar-light  ">
  <div class="container-fluid">
  <a class="navbar-brand" href="#">
      <img src="./assets/imagenes/layout_set_logo.png" alt="" width="100%"  class="d-inline-block align-top">
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link " aria-current="page" href="inicio.php">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link " href="ideas.php">ideas</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="anteproyecto.php">anteproyecto</a>
        </li>
        <li class="nav-item">
          <a class="nav-link " href="videos.php" >videos</a>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="assets/imagenes/web-3967926_1280.jpg" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="assets/imagenes/img2.png" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="assets/imagenes/webdesigner-2443766_1280.jpg" class="d-block w-100" alt="...">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </a>
</div>

<section class="page-section about-heading">
    <div class="container">
     <br>
      <div class="about-heading-content">
        <div class="row">
          <div class="col-xl-9 col-lg-10 mx-auto">
            <div class="bg-faded rounded p-3">
              <h2 class="section-heading mb-2">
                <span class="section-heading-upper">¿Qué es un anteproyecto?</span>
              </h2>
                <p>El anteproyecto es un documento en el que se redacta una propuesta de proyecto con el fin de presentar una idea organizada a un profesor (asesor) que sugerirá cambios y modificaciones hasta lograr que el anteproyecto cumpla con los siguientes elementos:

              <ol>
                <li><b> Titulo,autor(es)y director</b></li>
                <p> Definida la idea o el tema específico de interés para la investigación, es necesario
                    condensarlo (sintetizarlo) en una frase que exprese la esencia de la idea o el tema que
                    va a investigarse. Es importante tener en cuenta el criterio de que “a mayor extensión
                    menor comprensión y viceversa”.</p>
                <p> El título debe demostrar el tema y, en particular, el problema que va a investigarse,
                    que igualmente debe reflejarse en todo el proceso del desarrollo del estudio.
                </p>
                <li> <b> Tabla de contenido</b></li>
                <p>Una tabla de contenido es un reflejo de la estructura de un documento y contiene los títulos de los temas y subtemas que forman el documento.</p>
                <li> <b>Resumen</b></li>
                <p>El resumen es una exposición corta y clara del tema desarrollado, de la metodología utilizada, los resultados obtenidos y las conclusiones a que se ha llegado. No debe exceder las trescientas (300) palabras escritas a un espacio</p>
                <li> <b> Plateamiento del problema</b></li>
                <p>Todo problema aparece a raíz de una dificultad y esta se origina a partir de una
                    necesidad, en el cual aparecen las dificultades sin resolver. De ahí la necesidad de
                    hacer un planteamiento adecuado del problema a fin de no confundir efectos
                    secundarios del problema a investigar con la necesidad del problema a investigar. <br>
                    El planteamiento del problema, va a establecer la dirección del estudio para lograr
                    ciertos objetivos, debe hacerse con mucha objetividad centrándose en forma directa
                    con el tema que se va a desarrollar.
                </p>
                <li><b>Objetivos(general y especiificos)</b></li>
                <p>Cuando se ha seleccionado el tema de investigación debe procederse a formular los
                    objetivos de investigación.
                    El objetivo de la investigación es el enunciado claro y preciso de las metas que se
                    persiguen.
                    Todo trabajo de investigación es evaluado por el logro de los objetivos, mediante un
                    proceso sistemático, los cuales deben haber sido previamente señalados y
                    seleccionados al comienzo de la investigación; los objetivos tienen que ser revisados
                    en cada una de las etapas del proceso; el no hacerlo puede ocasionar fallas en la
                    investigación, con la misma intensidad en que se presenten fallas en los objetivos.
                    Al final de la investigación, los objetivos han de ser identificados con los resultados, es
                    decir, toda la investigación deberá responder a los objetivos propuestos. <br>
                    <b>OBJETIVO GENERAL:</b>  Consiste en lo que se pretende realizar en la
                    investigación; el enunciado debe ser claro y preciso de las metas que se persiguen en
                    la investigación. El logro del objetivo general se obtiene con la formulación y obtención
                    de los objetivos específicos.
                    Para su redacción se empieza con un verbo en infinitivo, indicando la acción que se
                    piensa realizar. Ej: Plantear, crear, analizar, diseñar, conocer, implementar,
                    determinar, comprobar, evaluar etc. <br>
                    <b>OBJETIVOS ESPECÍFICOS:</b>  Indican lo que se pretende realizar en cada una de
                    las etapas de la investigación. Estos objetivos ayudan a lograr el objetivo general y
                    deben ser evaluados en cada paso para conocer los distintos niveles de resultados.
                    Los objetivos específicos son los que se investigan y no el objetivo general, ya que
                    éste se logra como resultado.
                    </p>
                <li><b> Justificacion e impacto central del proyecto</b></li>
                <p>Cuando ya se han realizado las etapas anteriores, se deben establecer las
                    motivaciones que llevan al investigador a desarrollar el proyecto. Para alcanzar esta
                    motivación se debe responder a la pregunta ¿por qué se investiga?.
                    La justificación de una investigación puede ser de carácter teórico, metodológico o
                    práctico.
                </p>
                <li><b> Marco referencial o marco teorico y antecedentes</b></li>
                <b>MARCO REFERENCIAL. </b><br> <br>
                <b> 1. MARCO HISTORICO.</b> 
                <p>La revisión bibliográfica es de mucha ayuda, y debe hacerse en una forma racional y
                    sistemática, empezando por las obras más generales, recientes y sencillas y seguir
                    con las más complejas y antiguas. Los antecedentes constituyen las fuentes
                    secundarias y la bibliografía sobre las cuales se podrá diseñar la investigación
                    propuesta, concretando su aporte al tema de investigación, resaltándolo con citas de
                    pie de página. La lectura de textos, libros especializados, revistas, trabajos anteriores,
                    tesis de grado, es de gran importancia en su formulación. De la misma manera la
                    capacidad de síntesis y comprensión del investigador</p>
                    <br>
                    <b>2. MARCO TEORICO.</b>
                    <p>Su objetivo es ubicar el tema dentro de un conjunto de teorías existentes para precisar
                        en cuál corriente de pensamiento se inscribe. Además se detallan las teorías que se
                        utilizarán directamente en el desarrollo de investigación. Por ejemplo teorías de:
                        Sistemas, proceso administrativo, leyes de oferta y demanda, calidad de vida,
                        satisfacción de necesidades sociales, teorías de la administración, tendencias
                        pedagógicas, de control, del valor, principios de contabilidad y de auditoría 
                        generalmente aceptados, psicosocial, asociatividad, técnicas gerenciales modernas:
                        Benchmarking, reingeniería, calidad total, justo a tiempo, planeación estratégica, joint
                        venture, outsourcing.
                        Se debe determinar cuál es su aporte a la investigación, resaltando el autor (es) a
                        través de citas de pie de página. </p>
                    <br>
                    <b>3. MARCO CONCEPTUAL.</b>
                    <p>Consiste en seleccionar los términos técnicos más frecuentes que se van a manejar en
                        el desarrollo del proyecto, con el fin de hacer más fácil y comprensible el trabajo para
                        todos los interesados. Se deben organizar en orden alfabético.
                    </p>
                    <br>
                    <b>4. MARCO LEGAL.</b>
                    <p>Es el articulado o soporte jurídico que avalan y/o respaldan el desarrollo del tema
                        objeto de estudio de acuerdo a lo establecido en las leyes fundamentales del estado y,
                        que en su orden están constituidos por: La constitución política, las leyes, decretos,
                        resoluciones, acuerdos y todos aquellos tratados o convenios de índole regional o
                        internacional pertinentes.</p>
                <li> <b>Metodologia</b> </li>
                <p> Metodología es el procedimiento ordenado que se sigue para establecer lo significativo
                    de los hechos y fenómenos hacia los cuales está encaminado el interés de la
                    investigación. Científicamente la metodología es un procedimiento general para lograr,
                    de una manera, el objetivo de la investigación.</p>
                    <b>1.TIPO DE INVESTIGACIÒN.</b><br>
                    <p>Es conveniente determinar el tipo de estudio o investigación que se va a realizar, ya
                        que cada uno de éstos requiere una estrategia diferente.
                        Las investigaciones o estudios pueden ser: históricos, descriptivos, experimentales,
                        exploratorio, explicativa, correlacional, u otra que el investigador estime conveniente.</p>
                    <b>2. POBLACIÒN</b><br>
                    <p>Es el conjunto que se tomará y para el que serán válidas las conclusiones del trabajo
                        (personas, institucionales, cosas). Pueden existir varios universos, los que se deben
                        cuantificar y detallar para así captar sobre quiénes se va a trabajar realmente. Se
                        deben hacer cuadros explicativos cuantificados.</p>
                    <b>3. MUESTRA</b><br>
                    <p>Por cuestiones de economía, tiempo y dinero es necesario trabajar con un grupo
                        menor al definido en la población, señalando el método (empírico, o estadístico) para
                        la selección de la muestra. También se debe cuantificar y detallar la muestra, para
                        mayor claridad. Se deben hacer cuadros explicativos cuantificados. </p>
                <li><b>Participantes</b> </li>
                <p>Son todoas las personas que estan involucradas en el proyecto</p>
                <li> <b> Recursos de apoyo para la investigacion</b></li>
                <p>Un recurso es todo lo que se necesita para ejecutar una tarea o proyecto.</p>
                <li>Resultados esperados</li>
                <li>Cronograma</li>
                <li>Bibliografia</li>
              </ol>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<footer class="footer text-faded text-center py-3">
    <div class="container">
      <p class="m-0 small">Copyright &copy; UNIMINUTO 2021</p>
      <p class="m-0 small">Institución de Educación Superior sujeta a inspección y vigilancia por el Ministerio de Educación Nacional</p>
      <p class="m-0 small">Personería jurídica: Resolución 10345 del 1 de agosto de 1990 MEN</p>
    </div>
</footer>
    
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" ></script>
</body>
</html>