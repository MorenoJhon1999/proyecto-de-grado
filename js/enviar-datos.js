$(document).ready(function(){
    $('#registrar').click(function(){
      var datos = $('#frregistro').serialize();
        $.ajax({
            type: "POST",
            url:"insertar-registro.php",
            data:datos,
            success:function(r){
                if(r==1){
                   alerta();
                }
                else{
                   alerta2();
                }
            }
        });
        return false;
    });
});

function alerta(){
    Swal.fire(
        'Usuario Registrado Correctamente',
        'Uniminuto',
        'success'
      )
}

function alerta2(){
    Swal.fire(
        'OHHHHH  HA OCURRIDO UN HERROR!',
        'El Usuario ya esta registrado || error de conexion || Por favor llenar los campos',
        'error'
      )
}

/***********************************************************************/

$(document).ready(function(){
    $('#Enviar').click(function(){
      var datos = $('#frideas').serialize();
      alert(datos);
      return false;
        $.ajax({
            type: "POST",
            url:"insertar-ideas.php",
            data:datos,
            success:function(r){
                if(r==1){
                   alerta();
                }
                else{
                   alerta2();
                }
            }
        });
        return false;
    });
});


/***********************************************************************/


/***********************************************************************/

$(document).ready(function(){
    $('#RegistrarAS').click(function(){
      var datos = $('#ASasesor').serialize();
        $.ajax({
            type: "POST",
            url:"asignacion-asesor.php",
            data:datos,
            success:function(r){
                if(r==1){
                   alerta5();
                }
                else{
                   alerta6();
                }
            }
        });
        return false;
    });
});

function alerta5(){
    Swal.fire(
        'La asignacion del asesor al proyecto fue exitosa y se Actualizo las horas del Asesor',
        'Uniminuto',
        'success'
      )
}

function alerta6(){
    Swal.fire(
        'OHHHHH  HA OCURRIDO UN HERROR!',
        'Al asesor se les acabaron las horas de asesoria || Error de conexion',
        'error'
      )
}

/***********************************************************************/



/***********************************************************************/

$(document).ready(function(){
    $('#RegistrarP').click(function(){
      var datos = $('#Roproyecto').serialize();
        $.ajax({
            type: "POST",
            url:"insertar-proyecto.php",
            data:datos,
            success:function(r){
                if(r==1){
                 alerta3();
                }
                else {
                    alerta4();
                 }
            }
        });
        return false;
    });
});

function alerta3(){
    Swal.fire({
        title: 'Esta Segura de?',
        text: "Guardar la informacion del Proyecto",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Guardar'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'Retroalimentacion registrada con exito',
        'Uniminuto',
        'success'
          )
        }
      })
}


function alerta4(){
    Swal.fire(
        'OHHHHH  HA OCURRIDO UN HERROR!',
        'Completa todos los campos || Error de conexion',
        'error'
      )
}

/***********************************************************************/

$(document).ready(function(){
    $('#EnviarObretroalimentacion').click(function(){
      var datos = $('#Obretroalimentacion').serialize();
        $.ajax({
            type: "POST",
            url:"create_retroalimentacion.php",
            data:datos,
            success:function(r){
                if(r==1){
                   alerta7();
                }
                else{
                   alerta8();
                }
            }
        });
        return false;
    });
});

function alerta7(){
    Swal.fire(
        'Retroalimentacion registrada con exito',
        'Uniminuto',
        'success'
      )
}

function alerta8(){
    Swal.fire(
        'OHHHHH  HA OCURRIDO UN HERROR!',
        'El proyecto ya tiene un asesor asignado ya esta registrado o error de conexion',
        'error'
      )
}

/***********************************************************************/



/*************************Asignar horas asesor**********************************************/

$(document).ready(function(){
    $('#RegistrarHorasAsesor').click(function(){
      var datos = $('#HorasSasesor').serialize();
      
        $.ajax({
            type: "POST",
            url:"insertar-horas-asesor.php",
            data:datos,
            success:function(r){
                if(r==1){
                    alertahoras();
                }
                else{
                    alertahorasnull();
                }
            }
        });
        return false;
    });
});


function alertahoras(){
    Swal.fire({
        title: 'Esta Segura de?',
        text: "Guardar la informacion",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Guardar'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'Las horas fueron asignada al Profesor con exito',
        'Uniminuto',
        'success'
          )
        }
      })
}


function alertahorasnull(){
    Swal.fire(
        'OHHHHH  HA OCURRIDO UN HERROR!',
        'Completa todos los campos || Error de conexion',
        'error'
      )
}

/*************************Asignar horas jurado asesor**********************************************/

$(document).ready(function(){
    $('#RegistrarHorajurado').click(function(){
      var datos = $('#AHjurado').serialize();
     
        $.ajax({
            type: "POST",
            url:"insertar-horas-jurado.php",
            data:datos,
            success:function(r){
                if(r==1){
                    alertahoras();
                }
                else{
                    alertahorasnull();
                }
            }
        });
        return false;
    });
});


function alertahoras(){
    Swal.fire({
        title: 'Esta Segura de?',
        text: "Guardar la informacion",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Guardar'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'Las horas fueron asignada al Profesor con exito',
        'Uniminuto',
        'success'
          )
        }
      })
}


function alertahorasnull(){
    Swal.fire(
        'OHHHHH  HA OCURRIDO UN HERROR!',
        'Completa todos los campos || Error de conexion',
        'error'
      )
}

/***********************************************************************/

$(document).ready(function(){
  $('#RegistrarASPROYECTO').click(function(){
    var datos = $('#ASasesorPROYECTO').serialize();
      $.ajax({
          type: "POST",
          url:"insertar-asesor-proyecto.php",
          data:datos,
          success:function(r){
              if(r==1){
                  alertahoras();
              }
              else{
                  alertahorasnull();
              }
          }
      });
      return false;
  });
});


function alertahoras(){
  Swal.fire({
      title: 'Esta Segura de?',
      text: "Guardar la informacion",
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Guardar'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'La asignacion del asesor al proyecto fue exitosa y se Actualizo las horas del Asesor',
      'Uniminuto',
      'success'
        )
      }
    })
}


function alertahorasnull(){
  Swal.fire(
      'OHHHHH  HA OCURRIDO UN HERROR!',
      'Completa todos los campos || Error de conexion',
      'error'
    )
}

/***********************************************************************/

$(document).ready(function(){
  $('#RegistrarASjuradoPROYECTO').click(function(){
    var datos = $('#AjuradoPROYECTO').serialize();
   
      $.ajax({
          type: "POST",
          url:"jurado.php",
          data:datos,
          success:function(r){
              if(r==1){
                  alertahoras();
              }
              else{
                  alertahorasnull();
              }
          }
      });
      return false;
  });
});


function alertahoras(){
  Swal.fire({
      title: 'Esta Segura de?',
      text: "Guardar la informacion",
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Guardar'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'La asignacion del Jurado al proyecto fue exitosa ',
      'Uniminuto',
      'success'
        )
      }
    })
}


function alertahorasnull(){
  Swal.fire(
      'OHHHHH  HA OCURRIDO UN HERROR!',
      'Completa todos los campos || Error de conexion',
      'error'
    )
}