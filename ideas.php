<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>IDEAS</title>
    <script src="https://kit.fontawesome.com/9e00248cd3.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/bootstrap-material-design.min.css">
    <link rel="stylesheet" href="./css/micrositio.css">
</head>
<body>
<nav id="inicio" class="navbar navbar-expand-lg navbar-light  ">
  <div class="container-fluid">
  <a class="navbar-brand" href="#">
      <img src="./assets/imagenes/layout_set_logo.png" alt="" width="100%"  class="d-inline-block align-top">
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link " aria-current="page" href="inicio.php">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="ideas.php">ideas</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="anteproyecto.php">anteproyecto</a>
        </li>
        <li class="nav-item">
          <a class="nav-link " href="videos.php" >videos</a>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="assets/imagenes/code-1076536_1280.jpg" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="assets/imagenes/computer-1245714_1280.jpg" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="assets/imagenes/typing-849806_1280.jpg" class="d-block w-100" alt="...">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </a>
</div>

    <div class="text-center">
    <h2 class="title-one ">¿Qué es una idea de anteproyecto?</h2>
     </div>
        <p class="texto-one">Una idea de anteproyecto es una iniciativa general que tiene el estudiante como base para realizar su trabajo de grado,
             pero antes debe pulirse con la ayuda de un profesor para que después se pueda plasmar en un anteproyecto.</p>

    <h3 class="title-there">¿Qué elementos debe tener su idea de anteproyecto?</h3>
    <div class="container-fluid">
  <div class="row">
    <div class="col-6">
    <p>Para iniciar la depuración de la idea, el estudiante debe presentarle al estudiante los siguientes elementos:</p>
<ol>
  <li><b>Título de la idea:</b> el título debe cumplir con los siguientes requisitos:</li>
    <p>
    • Es preciso, atractivo e informativo.<br>
    • Permite identificar la naturaleza del trabajo y métodos a utilizar.<br>
    • Evita el uso abreviaturas.<br>
    • Su extensión no rebasa las 20 palabras.<br>
  </p>
  <li><b>Palabras clave:</b> las palabras clave son 3, 4 o 5 palabras, términos o descriptores que permiten identificar fácilmente el tema seleccionado por el estudiante.<br><br>
  </li>
  <li><b>Planteamiento del problema:</b> debe describir la situación problemática de forma precisa, preferiblemente con datos y/o estadísticas reales.<br><br>
  </li>
  <li><b>Objetivo general:</b> el objetivo general debe cumplir con los siguientes requisitos</li>
  <p>
        • Describe brevemente lo que se desea lograr con el proyecto de grado.<br>
        • Inicia con un verbo en infinitivo, en este caso, generalmente es: desarrollar.<br>
        • No sobrepasa las 25 palabras.<br>
  
    </div>
    <div class="col-6 ">
    <iframe width="100%" height="400" src="https://www.youtube.com/embed/GMXyKTxImdI"></iframe>
    </div>
  </div>
</div>

    <div class="container-fluid">
  
  <li><b>Objetivos específicos:</b> se deben redactar entre 3 a 4 objetivos específicos que cumplan con los siguientes requisitos:
  
        • Son los resultados o metas que se deben obtener para lograr el objetivo general.<br>
        • Son medibles.<br>
        • Inician con el verbo en infinitivo como: analizar, diseñar, desarrollar, etc.<br>
        • Los objetivos deben cumplir con la <a target= "_blank" class="link" href="http://eduteka.icesi.edu.co/articulos/TaxonomiaBloomDigital" target="_blank">Taxonomia de Bloom </a> para la era digital.<br><br>
  <li><b>Justificación:</b> explica porqué es importante el proyecto.</li>
</ol> 
</div>
</div>
</div>
</div>
</div>

<footer class="footer text-faded text-center py-3">
    <div class="container">
      <p class="m-0 small">Copyright &copy; UNIMINUTO 2021</p>
      <p class="m-0 small">Institución de Educación Superior sujeta a inspección y vigilancia por el Ministerio de Educación Nacional</p>
      <p class="m-0 small">Personería jurídica: Resolución 10345 del 1 de agosto de 1990 MEN</p>
    </div>
</footer>
    
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" ></script>
</body>
</html>