<!-- Page content -->
<section class="full-box page-content">
			<nav class="full-box navbar-info">
				<a href="#" class="float-left show-nav-lateral">
					<i class="fas fa-exchange-alt"></i>
				</a>
				<a href="user-update.php?usertype=2&userid=<?=$userid?>&estadoid=<?=$idea_estado;?>">
					<i class="fas fa-user-cog"></i>
				</a>
				<a href="http://localhost/Gestion-de-Anteproyectos-and-Proyectos-de-Grado%20Uniminuto/index.php?cerrar-session=1" class="btn-exit-system">
					<i class="fas fa-power-off"></i>
				</a>
			</nav>

			<!-- Page header -->
			<div class="full-box page-header">
				<h3 class="text-left">
					<i class="fab fa-dashcube fa-fw"></i> &nbsp; DASHBOARD
				</h3>
				<p class="text-justify">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit nostrum rerum animi natus beatae ex. Culpa blanditiis tempore amet alias placeat, obcaecati quaerat ullam, sunt est, odio aut veniam ratione.
				</p>
			</div>
			
			<!-- Content -->
			<div class="full-box tile-container">

				<a href="registro-proyecto.php?registro-proyectos=3&userid=<?=$userid;?>" class="tile">
					<div class="tile-tittle">Registro  Proyecto </div>
					<div class="tile-icon">
						<img src="assets/avatares/nota.png" width="150" height="120" alt="">
						<p>5 Registrados</p>
					</div>
				</a>


				<a href="consulta-ideas.php?usertype=2&userid=<?=$userid;?>" class="tile">
					<div class="tile-tittle">Consulta de Proyectos</div>
					<div class="tile-icon">
                    <img src="assets/avatares/investigacion.png" width="150" height="120" alt="">
						<p>50 Registrados</p>
					</div>
				</a>

				
                
               
				
			</div>