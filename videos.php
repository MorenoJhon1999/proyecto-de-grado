<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Videos</title>
    <script src="https://kit.fontawesome.com/9e00248cd3.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/bootstrap-material-design.min.css">
    <link rel="stylesheet" href="./css/micrositio.css">
</head>
<body>
<nav id="inicio" class="navbar navbar-expand-lg navbar-light  ">
  <div class="container-fluid">
  <a class="navbar-brand" href="#">
      <img src="./assets/imagenes/layout_set_logo.png" alt="" width="100%"  class="d-inline-block align-top">
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link " aria-current="page" href="inicio.php">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link " href="ideas.php">ideas</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="anteproyecto.php">anteproyecto</a>
        </li>
        <li class="nav-item">
          <a class="nav-link  active" href="videos.php" >videos</a>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div class="container-fluid">
  <div class="row">
    <div class="col-6">
    <iframe width="100%" height="100%" src="https://www.youtube.com/embed/1TOYS41ULsc"></iframe>
    </div>
    <div class="col-6 ">
    <iframe width="100%" height="400" src="https://www.youtube.com/embed/WSUxYaZaE-g"></iframe>
    </div>
    <br>
    <div class="col-6 ">
    <iframe width="100%" height="400" src="https://www.youtube.com/embed/JiVbQFSJ8ss" ></iframe>
    </div>
    <div class="col-6 ">
    <iframe width="100%" height="100%" src="https://www.youtube.com/embed/qbBYkw7G2rM" ></iframe>
    </div>
  </div>
</div>


      <footer class="footer text-faded text-center py-3">
    <div class="container">
      <p class="m-0 small">Copyright &copy; UNIMINUTO 2021</p>
      <p class="m-0 small">Institución de Educación Superior sujeta a inspección y vigilancia por el Ministerio de Educación Nacional</p>
      <p class="m-0 small">Personería jurídica: Resolución 10345 del 1 de agosto de 1990 MEN</p>
    </div>
</footer>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" ></script>
</body>
</html>