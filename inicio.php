<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Modulo Informativo</title>
<script src="https://kit.fontawesome.com/9e00248cd3.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="./css/bootstrap.min.css">
<link rel="stylesheet" href="./css/bootstrap-material-design.min.css">
<link rel="stylesheet" href="./css/micrositio.css">
</head>
<body>
<nav id="inicio" class="navbar navbar-expand-lg navbar-light  ">
  <div class="container-fluid">
  <a class="navbar-brand" href="#">
      <img src="./assets/imagenes/layout_set_logo.png" alt="" width="100%"  class="d-inline-block align-top">
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="inicio.php">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="ideas.php">ideas</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="anteproyecto.php">anteproyecto</a>
        </li>
        <li class="nav-item">
          <a class="nav-link " href="videos.php" >Videos</a>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="assets/imagenes/intro1.jpg" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="assets/imagenes/student-849825_1280.jpg" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="assets/imagenes/typing-849806_1280.jpg" class="d-block w-100" alt="...">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </a>
</div>

    <div class="container">
        <div class="intro">
         <div class="text-center">
            <h2 class="title-one">¿Qué es proyecto de grado?</h2>
            </div>
            <p class="texto-one">Según el  <a target ="_blank" class="link" href="documentos/acuerdo005.pdf">Acuerdo No.005 </a> del 7 de julio de 2015 en el que se aprueban y promulgan los lineamientos de proceso y procedimientos para la aplicación de opción de grado: Un proyecto de grado es un trabajo de tipo investigativo,tecnologico y/o tecnico ofrecido por un programa(presencial y/0 a distancia) ofertado por UNIMINUTO o sus convenios,
                 realizado por un estudiante en su ultimo semestre,para optar al titulo porfesional,tecnologico o tecnico.</p>
            <div class="text-center">
             <a class="btn btn-primary " href="#inicio">Vuelve al inicio</a>
            </div>
        </div>
    </div>

  <br><br><br>
    <div class="container-fluid">
  <div class="row">
    <div class="col-6 ">
    <div class="title-two">¿Cuáles son las fases que se deben realizar para presentar un trabajo de grado?</div>
    <p class="texto-two" >Este proyecto,en culaquiera de las modalidades mencionadas,contempla generalmente tres fases(para el caso de los progrmas de potgrado,solo se contemplaran las fases dos y tres):</p>
      <br>
      <li class= "items" ><b>FASE 1:</b> Presentacion del anteptoyecto.</li>
      <li class= "items" ><b>FASE 2:</b> Presentacion del informe final.</li>
      <li class= "items" ><b>FASE 3:</b> Socializacion.</li>
    </div>
    <div class="col-6 ">
    <iframe width="100%" height="100%" src="https://www.youtube.com/embed/9nLbxqBzxK0" ></iframe>
    </div>
  </div>
</div>

   
<footer class="footer text-faded text-center py-3">
    <div class="container">
      <p class="m-0 small">Copyright &copy; UNIMINUTO 2021</p>
      <p class="m-0 small">Institución de Educación Superior sujeta a inspección y vigilancia por el Ministerio de Educación Nacional</p>
      <p class="m-0 small">Personería jurídica: Resolución 10345 del 1 de agosto de 1990 MEN</p>
    </div>
  </footer>
    
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" ></script>
</body>
</html>