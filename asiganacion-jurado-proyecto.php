<?php
	session_start();

	if(!isset($_SESSION['rol'])){
		header('Location: index.php');
	}else{

		if($_SESSION['rol'] != 1){
			header('Location: index.php');
		}
	}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Asignacion de Asesor Proyecto</title>
	<script src="https://kit.fontawesome.com/9e00248cd3.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="./css/normalize.css">
	<link rel="stylesheet" href="./css/bootstrap.min.css">
	<link rel="stylesheet" href="./css/bootstrap-material-design.min.css">
	<link rel="stylesheet" href="./css/all.css">
	<link rel="stylesheet" href="./css/sweetalert2.min.css">
	<script src="./js/sweetalert2.min.js" ></script>
	<link rel="stylesheet" href="./css/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" href="./css/style.css">
</head>
<body>

     <!-- Main container -->
	<main class="full-box main-container">
		<!-- Nav lateral -->
		<?php include ('nav-lateral-coordinador.php') ?>
		<!-- Page content -->
		<section class="full-box page-content">
			<nav class="full-box navbar-info">
				<a href="#" class="float-left show-nav-lateral">
					<i class="fas fa-exchange-alt"></i>
				</a>
				<a href="user-update.php?usertype=1&userid=<?=$userid;?>">
					<i class="fas fa-user-cog"></i>
				</a>
				<a href="#" class="btn-exit-system">
					<i class="fas fa-power-off"></i>
				</a>
			</nav>

			<!-- Page header -->
			<div class="full-box page-header">
				<h3 class="text-left">
					<i class="fas fa-plus fa-fw"></i> &nbsp; ASIGANACION DE JURADO A  PROYECTO DE GRADO
				</h3>
				<p class="text-justify">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem odit amet asperiores quis minus, dolorem repellendus optio doloremque error a omnis soluta quae magnam dignissimos, ipsam, temporibus sequi, commodi accusantium!
				</p>
			</div>

			


			
			<!-- Content here-->
			<div class="container-fluid">
				<form id="AjuradoPROYECTO" method="POST" class="form-neon" autocomplete="off">
					<fieldset>
						<legend><i class="fas fa-user"></i> &nbsp; Información básica</legend>			
						<div class="container-fluid">
							<div class="row">
								<div class="col-12 col-md-12">
								<div class="form-group">
								<label for="inputSearch" class="bmd-label-floating">Ingrese el id del Asesor</label>
								<input type="text" class="form-control" name="idasesor" value="" id="myInput" maxlength="10">
									</div>
								</div>

								<div class="col-12 col-md-12">
									<div class="table-responsive">
										<table class="table table-dark table-sm">
										<thead>
										<tr class="text-center roboto-medium">
										<th>ID</th>
										<th>NOMBRES</th>
										<th>APELLIDOS</th>
										</tr>
										</thead>
										<tbody id="myTable">
										<?php include 'search-asesor.php'; ?>
										</tbody>						
										</table>
									</div>
								</div>

								<div class="col-12 col-md-12">
									<div class="form-group">
										<label for="cliente_nombre" class="bmd-label-floating">ID Proyecto:</label>
										<input type="text" class="form-control" name="idproyecto" id="idproyecto"  maxlength="40" required>
									</div>
								</div>

								<div class="col-12 col-md-12">
									<div class="table-responsive">
										<table class="table table-dark table-sm">
										<thead>
										<tr class="text-center roboto-medium">
										<th>ID_PROYECTO</th>
										<th>TITULO PROYECTO</th>
										<th>PALABRAS CLAVES</th>
										<th>ID ESTUDAINTE</th>
										<th>NOMBRES</th>
										<th>APELLIDOS</th>
										</tr>
										</thead>
										<tbody id="myTable">
										<?php include 'search-proyecto-grado.php'; ?>
										</tbody>						
										</table>
									</div>
								</div>
							
							</div>
						</div>
					</fieldset>
					<br><br><br>
					<p class="text-center" style="margin-top: 40px;">
						<button type="reset" class="btn btn-raised btn-secondary btn-sm"><i class="fas fa-paint-roller"></i> &nbsp; LIMPIAR</button>
						&nbsp; &nbsp;
						<button id="RegistrarASjuradoPROYECTO" class="btn btn-raised btn-info btn-sm"><i class="far fa-save"></i> &nbsp; GUARDAR</button>
					</p>
				</form>
			</div>	

		</section>
	</main>

	<script src="./js/sweetalert2.min.js" ></script>
    <script src="./js/jquery-3.5.1.min.js"></script>
    <script src="./js/enviar-datos.js"></script>
    <script src="./js/jquery-3.4.1.min.js" ></script>
	<script src="./js/popper.min.js" ></script>
	<script src="./js/bootstrap.min.js" ></script>
	<script src="./js/jquery.mCustomScrollbar.concat.min.js" ></script>
	<script src="./js/bootstrap-material-design.min.js" ></script>
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
	<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
	<script src="./js/main.js" ></script>
	<script src="./js/filtro.js"></script>
</body>
</html>