<?php
	include('conexion.php');

	session_start();

	if(isset($_GET['cerrar-session'])){
		session_unset();
		session_destroy();
		header("location:cerrar-session.php");
	}

	if(isset($_SESSION['rol'])){
		switch ($_SESSION['rol']) {
			case 1:
				header('Location: sala-coordinador.php');
				break;
			case 2:
				header('Location: sala-estudiantes.php');
				break;
			case 3:
				header('Location: sala-asesor.php');
				break;
			
			default:
				# code...
				break;
		}
	}

		if(isset($_POST['username']) && isset($_POST['password'])){
			$username = $_POST['username'];
			$password = $_POST['password'];

			$consulta = "SELECT * FROM  usuarios WHERE correo ='$username' AND  contraseña = '$password'";

			$resulltado = mysqli_query($conexion,$consulta);

			$row  = mysqli_fetch_assoc($resulltado);

			if($row > 0){


				$rol = $row["id_rol"];

				$idea_estado = $row['id_estado'];


				$_SESSION['rol'] = $rol;

				echo '<script>
						if(window.history.replaceState){
						window.history.replaceState(null,null,window.location.href);
						}
						</script>';

				switch ($_SESSION['rol']) {
					case 1:
						header("Location:sala-coordinador.php?usertype=1&userid=$username");
						break;
					case 2:
						header("Location: sala-estudiantes.php?usertype=2&userid=$username&estadoid=$idea_estado");
						break;
					case 3:
						header("Location: sala-asesor.php?usertype=3&userid=$username");
						break;
					
					default:
						# code...
						break;

				}

			}else{
			echo '<div class = "mensaje animate__animated animate__fadeInDown"> El usuario o contraseña son incorrectos </div>';
			}
		}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gestion de Anteproyectos y Proyectas de Grados Uniminuto</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="./css/normalize.css">
	<link rel="stylesheet" href="./css/bootstrap.min.css">
	<link rel="stylesheet" href="./css/bootstrap-material-design.min.css">
	<link rel="stylesheet" href="./css/all.css">
	<link rel="stylesheet" href="./css/sweetalert2.min.css">
	<link rel="stylesheet" href="./css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="./css/animate.min.css">
	<link rel="stylesheet" href="./css/style.css">
	<link rel="stylesheet" href="./css/login.css">
</head>
<body>
<div class="login-container-u">
		<div class="login-content-u">
			<div class="text-center">
                <a  href="https://www.uniminuto.edu"><img class="animate__animated animate__bounce logo-unuminuto"  src="assets/imagenes/logo.png" alt="logo-unuminuto"></a>
                <h2>SELECCIÓN DE IDEAS DE ANTEPROYECTOS Y PROYECTOS DE GRADO</h2>
                <p>Esta es una aplicación web desarrollada como parte del proyecto de grado de los estudiantes Jhon Stewar Moreno Murillo, Bryam Ali Quevedo Garcia y Andres eduardo Parra</p>
            </div>
            <div class="text-center">
				<button class=" ingresar" onclick="document.getElementById('id01').style.display='block'"style="width:auto;">INGRESAR</button>
			</div>
		</div>
    </div>
    <div id="id01" class="modal animate__animated animate__fadeInUpBig" >
        <div class="modal-content-u">
            <div class="row">
                <div class="col-sm-7 ">
                   <div class="contenedor-carrousel">
                        <!--inicio carrousel-->
                        <div id="demo" class="carousel slide" data-ride="carousel">
						<ul class="carousel-indicators">
						  <li data-target="#demo" data-slide-to="0" class="active"></li>
						  <li data-target="#demo" data-slide-to="1"></li>
						  <li data-target="#demo" data-slide-to="2"></li>
						</ul>
						<div class="carousel-inner">
						  <div class="carousel-item active">
							<img src="assets/imagenes/fondo-pricipal.jpg"  width="1100" height="500">
						  </div>
						  <div class="carousel-item">
							<img src="assets/imagenes/FondoCampus.jpg"  width="1100" height="500">
						  </div>
						  <div class="carousel-item">
							<img src="assets/imagenes/img1.jpg"  width="1100" height="500">
						  </div>
						  <div class="carousel-item">
							<img src="assets/imagenes/img4.jpg"  width="1100" height="500">
						  </div>
						</div>
						<!-- Left and right controls -->
						<a class="carousel-control-prev" href="#demo" data-slide="prev">
						  <span class="carousel-control-prev-icon"></span>
						</a>
						<a class="carousel-control-next" href="#demo" data-slide="next">
						  <span class="carousel-control-next-icon"></span>
						</a>
					  </div>
                        <!-- fin carrousel-->
                   </div>
                </div>

                <div class="col-sm-5 bg-warning">
                  <div class="contenedor-form">
                     <div class="close">
						<button type="button" onclick="document.getElementById('id01').style.display='none'"
						class="cancelbtn-u"><i class="bi bi-x-circle"></i></button>
                    </div>
                    <div class="text-center">
                        <a  href="https://www.uniminuto.edu"><img class="animate__animated animate__bounce logo-unuminuto-u"  src="assets/imagenes/logo.png" alt="logo-unuminuto"></a>
                    </div>

                    <form class="formulario-inicio" action="#" method="post" autocomplete="off">
                        <input class="control-button" type="email" name="username" type="text" placeholder="Usuario @Uuniminuto...">
                        <input class="control-button" name="password" type="password" placeholder="Contraseña">
                        <button class="control-button i" type="submit">Iniciar Sesión</button>
                    </form>
                  </div>
                </div>
            </div>
        </div>
	</div>


    <script src="./js/sweetalert2.min.js" ></script>
	<script src="./js/jquery-3.4.1.min.js" ></script>
	<script src="./js/popper.min.js" ></script>
	<script src="./js/bootstrap.min.js" ></script>
	<script src="./js/jquery.mCustomScrollbar.concat.min.js" ></script>
	<script src="./js/bootstrap-material-design.min.js" ></script>
	<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
	<script src="./js/main.js" ></script>
	<script>
		var modal = document.getElementById('id01');
		window.onclick = function (event) {
			if (event.target == modal) {
				modal.style.display = "none";
			}
		}
	</script>
</body>
</html>