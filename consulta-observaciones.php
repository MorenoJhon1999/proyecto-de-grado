<?php
	session_start();

	if(!isset($_SESSION['rol'])){
		header('Location: index.php');
	}else{

		if($_SESSION['rol'] != 2){
			header('Location: index.php');
		}
	}

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Consulta de observaciones</title>
    <script src="https://kit.fontawesome.com/9e00248cd3.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="./css/normalize.css">
	<link rel="stylesheet" href="./css/bootstrap.min.css">
	<link rel="stylesheet" href="./css/bootstrap-material-design.min.css">
	<link rel="stylesheet" href="./css/all.css">
	<link rel="stylesheet" href="./css/sweetalert2.min.css">
	<script src="./js/sweetalert2.min.js" ></script>
	<link rel="stylesheet" href="./css/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" href="./css/style.css">

</head>
<body>
	
	<!-- Main container -->
	<main class="full-box main-container">
		<!-- Nav lateral -->
		<?php include ('nav-lateral-estudiantes.php') ?>
		<!-- Page content -->
		<section class="full-box page-content">
			<nav class="full-box navbar-info">
				<a href="#" class="float-left show-nav-lateral">
					<i class="fas fa-exchange-alt"></i>
				</a>
				<a href="user-update.html">
					<i class="fas fa-user-cog"></i>
				</a>
				<a href="#" class="btn-exit-system">
					<i class="fas fa-power-off"></i>
				</a>
			</nav>

			<!-- Page header -->
			<div class="full-box page-header">
				<h3 class="text-left">
					<i class="fas fa-plus fa-fw"></i> &nbsp; Consulta de Observaciones
				</h3>
				<p class="text-justify">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem odit amet asperiores quis minus, dolorem repellendus optio doloremque error a omnis soluta quae magnam dignissimos, ipsam, temporibus sequi, commodi accusantium!
				</p>
			</div>

		
			
			<!-- Content here-->
			<div class="container-fluid"> 
            <form method="post" class="form-neon">
            <div class="container-fluid">
                <div class="row">
                <div class="col-12 col-md-6">
						<div class="form-group">
						    <label for="cliente_direccion" class="bmd-label-floating">ID del proyecto:</label>
							    <input type="number"  class="form-control" name="codigo_user" id="cliente_direccion" maxlength="150" required>
						</div>
                    </div>
                    <div class="col-12 col-md-6">
						<div class="form-group">
    
						 <button type="submit" class="btn btn-primary"> Buscar Observaciones</button>
							    
						</div>
					</div>
                </div>
            </div>
            </form>


				<form  method="POST" class="form-neon" autocomplete="off" enctype="multipart/form-data">
					<fieldset>
                        <legend><i class="fas fa-user"></i> &nbsp; Observaciones</legend>
						<div class="container-fluid">
                        <div class="row">
						<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="titulo" class="bmd-label-floating">Obervaciones del titilo</label>
								<textarea type="text"  class="form-control" name="titulo" id="titulo" required></textarea>
							</div>
						</div>
						<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="palabras_clave" class="bmd-label-floating">Correpciones del titulo</label>
								<textarea type="text" class="form-control" name="palabras_clave" id="palabras_clave" required></textarea>
							</div>
						</div>
						<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="planteamiento_problema" class="bmd-label-floating">Observaciones Palabras Claves</label>
								<textarea type="text" class="form-control" name="planteamiento_problema" id="planteamiento_problema" required></textarea>
							</div>
						</div>
						<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="objetivo_general" class="bmd-label-floating">Correpciones palabras Claves</label>
								<textarea type="text" class="form-control" name="objetivo_general" id="objetivo_general" required></textarea>
							</div>
						</div>
						<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="objetivos_especificos" class="bmd-label-floating">Observaciones Planteamiento del Problema</label>
								<textarea type="text" class="form-control" name="objetivos_especificos" id="objetivos_especificos" required></textarea>
							</div>
						</div>
						<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="justificacion" class="bmd-label-floating">Correpcioines Planteamiento del Problema</label>
								<textarea type="text" class="form-control" name="justificacion" id="justificacion" required></textarea>
							</div>
						</div>
						<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="objetivos_especificos" class="bmd-label-floating">Observaciones Obejetivo General</label>
								<textarea type="text" class="form-control" name="objetivos_especificos" id="objetivos_especificos" required></textarea>
							</div>
						</div>
						<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="justificacion" class="bmd-label-floating">Correpcioines Objetivo General</label>
								<textarea type="text" class="form-control" name="justificacion" id="justificacion" required></textarea>
							</div>
                        </div>
                        
                        <div class="col-12 col-md-6">
							<div class="form-group">
								<label for="objetivos_especificos" class="bmd-label-floating">Observaciones Objetivos Especificos</label>
								<textarea type="text" class="form-control" name="objetivos_especificos" id="objetivos_especificos" required></textarea>
							</div>
						</div>
						<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="justificacion" class="bmd-label-floating">Correpcioines Obejetivos Especificos</label>
								<textarea type="text" class="form-control" name="justificacion" id="justificacion" required></textarea>
							</div>
						</div>
                        
                        <div class="col-12 col-md-6">
							<div class="form-group">
								<label for="objetivos_especificos" class="bmd-label-floating">Observaciones JUstificacion</label>
								<textarea type="text" class="form-control" name="objetivos_especificos" id="objetivos_especificos" required></textarea>
							</div>
						</div>
						<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="justificacion" class="bmd-label-floating">Correpcioines justificacion</label>
								<textarea type="text" class="form-control" name="justificacion" id="justificacion" required></textarea>
							</div>
                        </div>
                        <div class="col-12 col-md-6">
							<label for="documento" class="bmd-label-floating">Descragar Documento</label>
							<input type="file" class="form-control" name="documento" id="documento" required></textarea>
                        </div>

                        <div class="col-12 col-md-6">
							<label for="documento" class="bmd-label-floating">Subir Documentos</label>
							<input type="file"  class="form-control" name="documento" id="documento" required></textarea>
                        </div>
					        </div>
						</div>
					</fieldset>
					<br><br><br>
					<p class="text-center" style="margin-top: 40px;">
						<button type="reset" class="btn btn-raised btn-secondary btn-sm"><i class="fas fa-paint-roller"></i> &nbsp; LIMPIAR</button>
						&nbsp; &nbsp;
						<button type="submit" name="Enviar" class="btn btn-raised btn-info btn-sm"><i class="far fa-save"></i> &nbsp; GUARDAR</button>
					</p>
				</form>
			</div>	

		</section>
	</main>

    <script src="./js/jquery-3.4.1.min.js" ></script>
	<script src="./js/popper.min.js" ></script>
	<script src="./js/bootstrap.min.js" ></script>
	<script src="./js/jquery.mCustomScrollbar.concat.min.js" ></script>
	<script src="./js/bootstrap-material-design.min.js" ></script>
	<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
	<script src="./js/main.js" ></script>
</body>
</html>