<?php
	session_start();

	if(!isset($_SESSION['rol'])){
		header('Location: index.php');
	}else{

		if($_SESSION['rol'] != 3){
			header('Location: index.php');
		}
	}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Consulta Asignacion</title>
    <script src="https://kit.fontawesome.com/9e00248cd3.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="./css/normalize.css">
	<link rel="stylesheet" href="./css/bootstrap.min.css">
	<link rel="stylesheet" href="./css/bootstrap-material-design.min.css">
	<link rel="stylesheet" href="./css/all.css">
	<link rel="stylesheet" href="./css/sweetalert2.min.css">
	<script src="./js/sweetalert2.min.js" ></script>
	<link rel="stylesheet" href="./css/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" href="./css/style.css">
</head>
<body>

<main class="full-box main-container">
		<!----------------------------------------- Nav lateral ------------------------------------->
		<?php
			include "nav-lateral-asesor.php";
		?>
		<!---------------------------------------- fin Nav lateral ------------------------------------->

		<!-- Page content -->
		<section class="full-box page-content">
			<nav class="full-box navbar-info">
				<a href="#" class="float-left show-nav-lateral">
					<i class="fas fa-exchange-alt"></i>
				</a>
				
				<a href="#" class="btn-exit-system">
					<i class="fas fa-power-off"></i>
				</a>
			</nav>

			<!-- Page header -->
			<div class="full-box page-header">
				<h1 class="text-left">
					<i class="far fa-eye"></i>  &nbsp; Consultar ideas asignadas
				</h1>
			</div>
			<?php
			include('conexion.php');

			$userid = $_GET['userid'];

			$sql = "SELECT nombres, apellidos, id FROM usuarios WHERE  correo = '$userid'";

			$resul = mysqli_query($conexion,$sql);

			$respuesta =  mysqli_fetch_assoc($resul);

			$nombre = $respuesta["nombres"];

			$apellido = $respuesta["apellidos"];

			$id =  $respuesta["id"];
			?>

			<!-- Content here-->
			<div class="container-fluid">
				<form class="form-neon" action="ver_asignacion.php?usertype=2&userid=<?=$userid;?>" method="POST">
					<div class="row justify-content-md-center">
						<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="inputSearch" class="bmd-label-floating"><b>ID Profesor </b></label>
								<input type="text" class="form-control" name="buscar" value="<?=$id;?>" id="inputSearch" readonly>
							</div>
						</div>
						<div class="col-12 col-md-6">
							<p class="text-center" style="margin-top: 40px;">
								<button type="submit" class="btn btn-raised btn-info" ><i class="fas fa-search"></i> &nbsp; BUSCAR</button>
							</p>
						</div>
					</div>
				</form>

				<div class="table-responsive">
					<table class="table table-dark table-sm">
						<thead>
							<tr class="text-center roboto-medium">
								<th>ID DEL PROYECTO</th>
								<th>TÍTULO DEL PROYECTO</th>
								<th>ESTADO</th>
							</tr>
						</thead>
						<tbody>
						<?php
						include 'read-asignacion.php';
						?>												
						</tbody>
					</table>
				</div>
				
			</div>

		</section>
	</main>
    <script src="./js/sweetalert2.min.js" ></script>
    <script src="./js/jquery-3.5.1.min.js"></script>
    <script src="./js/enviar-datos.js"></script>
    <script src="./js/jquery-3.4.1.min.js" ></script>
	<script src="./js/popper.min.js" ></script>
	<script src="./js/bootstrap.min.js" ></script>
	<script src="./js/jquery.mCustomScrollbar.concat.min.js" ></script>
	<script src="./js/bootstrap-material-design.min.js" ></script>
	<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
	<script src="./js/main.js" ></script>
    <script src="./js/filtro.js"></script>
</body>
</html>