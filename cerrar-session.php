<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cerrar Session</title>
    <script src="https://kit.fontawesome.com/9e00248cd3.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="./css/normalize.css">
	<link rel="stylesheet" href="./css/bootstrap.min.css">
	<link rel="stylesheet" href="./css/bootstrap-material-design.min.css">
	<link rel="stylesheet" href="./css/all.css">
    <link rel="stylesheet" href="./css/animate.min.css">
	<link rel="stylesheet" href="./css/sweetalert2.min.css">
	<script src="./js/sweetalert2.min.js" ></script>
	<link rel="stylesheet" href="./css/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" href="./css/style.css">
</head>
<body>
<div class="content-principal">
<div class="text-center">
<br>
<a  href="https://www.uniminuto.edu"><img class="animate__animated animate__bounce logo-unuminuto"  src="assets/imagenes/logo.png" width = "300"  alt="logo-unuminuto"></a>
</div>

    <div class="text-center">
        <p class="gracias animate__animated animate__heartBeat ">Gracias por ultilizar nuestra Aplicacion Web</p>
    </div>

    <div class="text-center">
   <a href="index.php"><button type="button" class="btn btn-warning animate__animated animate__headShake ">Regresar</button></a>
    </div>
</div>
<script src="./js/jquery-3.4.1.min.js" ></script>
	<script src="./js/popper.min.js" ></script>
	<script src="./js/bootstrap.min.js" ></script>
	<script src="./js/jquery.mCustomScrollbar.concat.min.js" ></script>
	<script src="./js/bootstrap-material-design.min.js" ></script>
	<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
	<script src="./js/main.js" ></script>
</body>
</html>