<?php
	include_once "conexion.php";

	$sql = "SELECT * FROM usuarios
            WHERE  id_rol = 2";
	$datos = mysqli_query ($conexion,$sql);

	$sql_estudiantes = "SELECT count(id) AS cantidad_estudiantes
						FROM usuarios
                        WHERE  id_rol = 2";

	$datos_estudiantes = mysqli_query ($conexion,$sql_estudiantes);
	$row_estudiantes = mysqli_fetch_array($datos_estudiantes);
	echo "Cantidad de estudiantes encontrados:<b>" . $row_estudiantes['cantidad_estudiantes'] . "</b>" ;
    echo "<br>";
	if (!$datos)
	{
		$error = mysqli_query($conexion,$sql) or die(mysqli_error($conexion));
		echo $error;
	}
	else
	{
		while($row = mysqli_fetch_array($datos))
		{
		?>
		<tr class="text-center text-white" >
			<td><?= $row['id'] ?></td>
			<td><?= $row["nombres"] ?></td>
			<td><?= $row['apellidos'] ?></td>
			<td><?= $row['correo'] ?></td>
			<td><?= $row['telefono'] ?></td>
		</tr>												
		<?php
		}	
	}
	$_POST['buscar'] = "";
?>