<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Consulta Proyectos</title>
    <script src="https://kit.fontawesome.com/9e00248cd3.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="./css/normalize.css">
	<link rel="stylesheet" href="./css/bootstrap.min.css">
	<link rel="stylesheet" href="./css/bootstrap-material-design.min.css">
	<link rel="stylesheet" href="./css/all.css">
	<link rel="stylesheet" href="./css/sweetalert2.min.css">
	<script src="./js/sweetalert2.min.js" ></script>
	<link rel="stylesheet" href="./css/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" href="./css/style.css">
</head>
<body>

<main class="full-box main-container">
		<!-- Nav lateral -->
		<?php
		$usertype = $_GET['usertype'];
		if ($usertype == 3)
			include "nav-lateral-asesor.php";
		elseif ($usertype == 2)
			include "nav-lateral-estudiantes.php";
		elseif ($usertype == 1)
			include "nav-lateral-coordinador.php";
			?>

		<!-- Page content -->
		<section class="full-box page-content">
			<nav class="full-box navbar-info">
				<a href="#" class="float-left show-nav-lateral">
					<i class="fas fa-exchange-alt"></i>
				</a>
				<a href="http://localhost/Gestion-de-Anteproyectos-and-Proyectos-de-Grado%20Uniminuto/index.php?cerrar-session=1" class="btn-exit-system">
					<i class="fas fa-power-off"></i>
				</a>
			</nav>

			<!-- Page header -->
			<div class="full-box page-header">
				<h3 class="text-left">
					<i class="fab fa-dashcube fa-fw"></i> &nbsp; Consulta de Proyectos de grados
				</h3>
				<p class="text-justify">
                En este módulo se pueden buascar los proyectos a partir de un texto ingresado
				</p>
			</div>

            	<!-- Content here-->
			<div class="container-fluid">
				<form class="form-neon" action="consulta_ideas.php?usertype=2&userid=<?=$usertype;?>&userid=<?=$userid;?>" method="post" >
					<div class="container-fluid">
						<div class="row justify-content-md-center">
							<div class="col-12 col-md-6">
								<div class="form-group">
									<label for="inputSearch" class="bmd-label-floating">Ingrese caracteres para filtrar.	</label>
									<input type="text" class="form-control" name="buscar" value="" autocomplete="off" id="myInput" maxlength="30">
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="table-responsive">
					<table class="table table-dark table-striped">
						<thead>
							<tr class="text-center roboto-medium">
								<th>TITULO PROYECTO</th>
								<th>PALABRAS CLAVES</th>
								<th>ID ESTUDIANTES</th>
								<th>NOMBRE ESTUDIANTES</th>
								<th>APELLIDO ESTUDIANTES</th>
								<th>NOMBRE ASESOR</th>
								<th>ESTADO PROYECTO</th>

							</tr>
						</thead>
						<?php
						include 'read-proyectos.php';
						?>
					</table>
				</div>
				
			</div>

		</section>
	</main>
    
    <script src="./js/sweetalert2.min.js" ></script>
    <script src="./js/jquery-3.5.1.min.js"></script>
    <script src="./js/enviar-datos.js"></script>
    <script src="./js/jquery-3.4.1.min.js" ></script>
	<script src="./js/popper.min.js" ></script>
	<script src="./js/bootstrap.min.js" ></script>
	<script src="./js/jquery.mCustomScrollbar.concat.min.js" ></script>
	<script src="./js/bootstrap-material-design.min.js" ></script>
	<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
	<script src="./js/main.js" ></script>
    <script src="./js/filtro.js"></script>
</body>
</html>