<?php
	include_once "conexion.php";

	$sql = "SELECT count(id_proyecto) AS cantidad_ideas
			FROM ideas";
	$ideas = mysqli_query ($conexion,$sql);
	$respuesta_ideas = mysqli_fetch_array($ideas);

    $sql = "SELECT count(id_retroalimentacion) AS cantidad_observaciones
			FROM retroalimentacion_idea";
	$observaciones = mysqli_query ($conexion,$sql);
	$observaciones = mysqli_fetch_array($observaciones);

    $sql = "SELECT count(estado) AS cantidad_aprobadas
				FROM retroalimentacion_idea
				WHERE estado LIKE '%Aceptado%'";
	$aprobadas = mysqli_query ($conexion,$sql);
	$aprobadas = mysqli_fetch_array($aprobadas);
        
	$sql = "SELECT count(estado) AS cantidad_rechazadas
				FROM retroalimentacion_idea
				WHERE estado LIKE '%Rechazado%'";
	$rechazadas = mysqli_query ($conexion,$sql);
	$rechazadas = mysqli_fetch_array($rechazadas);

	$sql = "SELECT count(estado) AS cantidad_en_revision
				FROM retroalimentacion_idea
				WHERE estado LIKE  '%revision%'";
	$en_revison = mysqli_query ($conexion,$sql);
	$en_revison = mysqli_fetch_array($en_revison);


	$cantidad_ideas = $respuesta_ideas['cantidad_ideas'];
	$sql = "SELECT round( (count(id_retroalimentacion)/$cantidad_ideas),0 ) AS avg_observaciones_por_idea
		FROM retroalimentacion_idea";
	$avg_observaciones = mysqli_query ($conexion,$sql);
	$avg_observaciones = mysqli_fetch_array($avg_observaciones);

	
	?>
		<tbody>
		<tr class="text-center text-white" >
			<td><?= $respuesta_ideas['cantidad_ideas'] ?></td>
			<td><?= $observaciones['cantidad_observaciones'] ?></td>
			<td><?= $rechazadas['cantidad_rechazadas'] ?></td>	
			<td><?= $aprobadas['cantidad_aprobadas'] ?></td>
			<td><?= $en_revison['cantidad_en_revision'] ?></td>
			<td><?= $avg_observaciones['avg_observaciones_por_idea'] ?></td>
		</tr>												
		</tbody>						
	<?php
?>
