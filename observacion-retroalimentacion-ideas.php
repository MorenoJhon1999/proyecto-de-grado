<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Observacion Retroalimentaciones</title>
    <script src="https://kit.fontawesome.com/9e00248cd3.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="./css/normalize.css">
	<link rel="stylesheet" href="./css/bootstrap.min.css">
	<link rel="stylesheet" href="./css/bootstrap-material-design.min.css">
	<link rel="stylesheet" href="./css/all.css">
	<link rel="stylesheet" href="./css/sweetalert2.min.css">
	<link rel="stylesheet" href="./css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="./css/animate.min.css">
	<link rel="stylesheet" href="./css/style.css">
	<link rel="stylesheet" href="./css/login.css">
</head>
<body>
    <!-- Main container -->
<main class="full-box main-container">
		<!-- Nav lateral -->
		<?php include ('nav-lateral-estudiantes.php') ?>

		<!-- Page content -->
		<section class="full-box page-content">
			<nav class="full-box navbar-info">
				<a href="#" class="float-left show-nav-lateral">
					<i class="fas fa-exchange-alt"></i>
				</a>
				<a href="user-update.html">
					<i class="fas fa-user-cog"></i>
				</a>
				<a href="http://localhost/Gestion-de-Anteproyectos-and-Proyectos-de-Grado%20Uniminuto/index.php?cerrar-session=1" class="btn-exit-system">
					<i class="fas fa-power-off"></i>
				</a>
			</nav>

			<!-- Page header -->
			<div class="full-box page-header">
				<h3 class="text-left">
					<i class="fab fa-dashcube fa-fw"></i> &nbsp; CONSULTA RETROALIMENTACION ANTEPROYECTOS
				</h3>
				<p class="text-justify">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit nostrum rerum animi natus beatae ex. Culpa blanditiis tempore amet alias placeat, obcaecati quaerat ullam, sunt est, odio aut veniam ratione.
				</p>
			</div>

      <?php
       include ('conexion.php');
       $userid = $_GET['userid'];
       $sql = " SELECT id FROM usuarios WHERE correo =  '$userid'";
       $resul = mysqli_query($conexion,$sql);
       $respuesta =  mysqli_fetch_assoc($resul);
       
       $id = $respuesta['id'];
      
      ?>

      <?php
						include_once "conexion.php";

						$sql_estudiantes = "SELECT id_proyecto AS id_proyecto
						FROM ideas  WHERE id = '$id'   ";
						$resultado = mysqli_query ($conexion,$sql_estudiantes);
						$resultado = mysqli_fetch_array(	$resultado);
        ?>    

    <!-- Content here-->
		<div class="container-fluid">
    <form action="observacion-retroalimentacion-ideas.php?usertype=2&userid=<?=$userid;?>" method="post">
      <div class="row">
	  
        <div class="col-12 col-md-6">
          <div class="form-group">
            <label for="cliente_apellido" class="bmd-label-floating">ID de la idea</label>
            <input type="number"  class="form-control" name="buscarOb" value="<?php echo $resultado['id_proyecto'] ?>" readonly id="buscarOb" maxlength="6" required>
          </div>
        </div>
        <div class="col-12 col-md-6">
          <div class="form-group">
            <p style="margin-top: 30px;">
              <button type="submit" class="btn btn-raised btn-info" ><i class="fas fa-search"></i> &nbsp; Buscar</button>
            </p>
          </div>  
        </div>
      </div>
    </form>

    <?php include("actualizar-ideas.php");?>

	<?php  
  
    if( isset($_POST['buscarOb']) )
    {

  
      $id_observacion = $_POST['buscarOb'];

      $max = " SELECT MAX(id_retroalimentacion) AS max_id
      from retroalimentacion_idea  where id_proyecto = $id_observacion";
      $resultados = mysqli_query ($conexion,$max);
      $rew = mysqli_fetch_array($resultados);
  
      $id_max = $rew['max_id'];


      $consulta ="SELECT *
                  FROM retroalimentacion_idea 
                  WHERE id_proyecto = '$id_observacion' and id_retroalimentacion = '$id_max'";
	  $datos = mysqli_query ($conexion,$consulta);

      $raw  = mysqli_fetch_assoc($datos);

      if( $raw <= 0)
      {
        echo "No se encontró información para la idea con ID: <b>" . $id_observacion . "</b>";
      }else{
          include_once "observacion-form-principal.php";
      } 
      mysqli_close($conexion);
    }
    ?>
      </div> <!-- cierre row-->
    </div> <!-- cierre container-->
  </section>
</main>

<script src="./js/sweetalert2.min.js" ></script>
    <script src="./js/jquery-3.5.1.min.js"></script>
    <script src="./js/enviar-datos.js"></script>
    <script src="./js/jquery-3.4.1.min.js" ></script>
	<script src="./js/popper.min.js" ></script>
	<script src="./js/bootstrap.min.js" ></script>
	<script src="./js/jquery.mCustomScrollbar.concat.min.js" ></script>
	<script src="./js/bootstrap-material-design.min.js" ></script>
	<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
	<script src="./js/main.js" ></script>
</body>
</html>