<form  action="?actulizar=2&userid=<?=$userid;?>" method="post" enctype="multipart/form-data">
<fieldset>
<legend><i class="fas fa-user"></i> &nbsp; Observacion Retroalimentaciones</legend>
<div class="container-fluid">
     <div class="row">

		<div class="col-12 col-md-6">
			<div class="form-group">
			<label for="titulo" class="bmd-label-floating"> Observacion Titilo del Proyecto</label>
            <textarea type="text" class="form-control" name="titulo" id="titulo" disabled><?=$raw["ob_titulo"];?></textarea>
		</div>
		</div>

		<div class="col-12 col-md-6">
			<div class="form-group">
				<label for="palabras_clave" class="bmd-label-floating">Correpcion Titulo Poryecto</label>
		    	<textarea type="text" class="form-control" name="correpcion_titulo" id="correpcion_titulo" required></textarea>
		</div>
		</div>

		<div class="col-12 col-md-6">
			<div class="form-group">
			    <label for="planteamiento_problema" class="bmd-label-floating">Observacion Palabras Claves</label>
                <textarea type="text" class="form-control" name="palabras_claves" id="palabras_claves" disabled><?=$raw["ob_palabras_claves"];?></textarea>
		</div>
		</div>

		<div class="col-12 col-md-6">
			<div class="form-group">
			<label for="objetivo_general" class="bmd-label-floating">Correpcion Palabras Claves</label>
			<textarea type="text" class="form-control" name="correpcion_palabras_claves" id="correpcion_palabras_claves" required></textarea>
		    </div>
		</div>

        <div class="col-12 col-md-6">
			<div class="form-group">
			<label for="planteamiento_problema" class="bmd-label-floating">Observacion Planteamiento del Problema</label>
            <textarea type="text" class="form-control" name="titulo" id="titulo" disabled><?=$raw["ob_planteamiento_problema"];?></textarea>
		</div>
		</div>

		<div class="col-12 col-md-6">
			<div class="form-group">
			<label for="objetivo_general" class="bmd-label-floating">Correpcion Planteamiento del Problema</label>
			<textarea type="text" class="form-control" name="correpcion_problema" id="correpcion_problema" required></textarea>
		    </div>
		</div>

        <div class="col-12 col-md-6">
			<div class="form-group">
			<label for="planteamiento_problema" class="bmd-label-floating" > Observacion Obejetivo General</label>
            <textarea type="text" class="form-control" name="titulo" id="titulo" disabled><?=$raw["ob_objetivo_general"];?></textarea>
		</div>
		</div>

		<div class="col-12 col-md-6">
			<div class="form-group">
			<label for="objetivo_general" class="bmd-label-floating">Correpcion Obejetivo General</label>
			<textarea type="text" class="form-control" name="correpcion_general" id="correpcion_general" required></textarea>
		    </div>
		</div>

        <div class="col-12 col-md-6">
			<div class="form-group">
			<label for="planteamiento_problema" class="bmd-label-floating"> Observacion Objetivos Especificos</label>
            <textarea type="text" class="form-control" name="titulo" id="titulo" disabled><?=$raw["ob_objetivo_especifico"];?></textarea>
		</div>
		</div>

		<div class="col-12 col-md-6">
			<div class="form-group">
			<label for="objetivo_general" class="bmd-label-floating">Correpcion Objetivos Especificos</label>
			<textarea type="text" class="form-control" name="correpcion_especifico" id="correpcion_especifico" required></textarea>
		    </div>
		</div>

        <div class="col-12 col-md-6">
			<div class="form-group">
			<label for="planteamiento_problema" class="bmd-label-floating"> Observacion Justificacion</label>
            <textarea type="text" class="form-control" name="titulo" id="titulo" disabled><?=$raw["ob_justificacion"];?></textarea>
		</div>
		</div>

		<div class="col-12 col-md-6">
			<div class="form-group">
			<label for="objetivo_general" class="bmd-label-floating">Correpcion Justificacion</label>
			<textarea type="text" class="form-control" name="correpcion_justificacion" id="correpcion_justificacion" required></textarea>
		    </div>
		</div>

		<div class="col-12 col-md-12">
			<div class="form-group">
			<label for="usr"><b>Estado:</b></label>
            <select class="form-control" disabled id="estado" name="estado">
            <option value="#"><?=$raw['estado']?></option>

            </select>
		</div>
		</div>

       
        <div class="col-12 col-md-6">
			<div class="form-group">
            <a class=" nav-item descargar"  target="_blank" href="documentos/<?=$raw['documento']?>"><i class="fas fa-download"></i> Descargar documento</a>
		    </div>
        </div>

        
		<div class="col-12 col-md-6">
				<label for="documento" class="bmd-label-floating">Seleccione un documento de texto</label>
				<input type="file" class="form-control" name="documento" id="documento" required></textarea>
        </div>


	    </fieldset>
		<br><br><br>
		<p class="text-center" style="margin-top: 40px;">
		<button type="reset" class="btn btn-raised btn-secondary btn-sm"><i class="fas fa-paint-roller"></i> &nbsp; LIMPIAR</button>
		&nbsp; &nbsp;
		<button type="submit" name="Enviar" class="btn btn-raised btn-info btn-sm"><i class="far fa-save"></i> &nbsp; GUARDAR</button>
		</p>
    </div>
</div>
</form>