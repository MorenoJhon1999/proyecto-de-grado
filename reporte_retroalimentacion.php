<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reporte retroalimentaciones</title>
    <script src="https://kit.fontawesome.com/9e00248cd3.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="./css/normalize.css">
	<link rel="stylesheet" href="./css/bootstrap.min.css">
	<link rel="stylesheet" href="./css/bootstrap-material-design.min.css">
	<link rel="stylesheet" href="./css/all.css">
	<link rel="stylesheet" href="./css/sweetalert2.min.css">
	<link rel="stylesheet" href="./css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="./css/animate.min.css">
	<link rel="stylesheet" href="./css/style.css">
	<link rel="stylesheet" href="./css/login.css">
</head>
<body>

<main class="full-box main-container">
		<!-- Nav lateral -->
		<?php include ('nav-lateral-asesor.php') ?>

		<!-- Page content -->
		<section class="full-box page-content">
			<nav class="full-box navbar-info">
				<a href="#" class="float-left show-nav-lateral">
					<i class="fas fa-exchange-alt"></i>
				</a>
				<a href="user-update.html">
					<i class="fas fa-user-cog"></i>
				</a>
				<a href="http://localhost/Gestion-de-Anteproyectos-and-Proyectos-de-Grado%20Uniminuto/index.php?cerrar-session=1" class="btn-exit-system">
					<i class="fas fa-power-off"></i>
				</a>
			</nav>

			<!-- Page header -->
			<div class="full-box page-header">
				<h3 class="text-left">
					<i class="fab fa-dashcube fa-fw"></i> &nbsp; CONSULTA RETROALIMENTACIONES 
				</h3>
				<p class="text-justify text-dark">
					Aqui el asesor podra consultar todas las retroalimentaciones que le ha hecho a un Anteproyecto
				</p>
			</div>

      

    <!-- Content here-->
		<div class="container-fluid">
    <form action="reporte_retroalimentacion.php?usertype=3&userid=<?=$userid;?>" method="post">
      <div class="row">
	  
        <div class="col-12 col-md-6">
          <div class="form-group">
            <label for="cliente_apellido" class="bmd-label-floating">ID de la idea</label>
            <input type="number"  class="form-control" name="buscarOb"  id="buscarOb" maxlength="6" required>
          </div>
        </div>
        <div class="col-12 col-md-6">
          <div class="form-group">
            <p style="margin-top: 30px;">
              <button type="submit" class="btn btn-raised btn-info" ><i class="fas fa-search"></i> &nbsp; Buscar</button>
            </p>
          </div>  
        </div>
      </div>
    </form>
    <div class="table-responsive">
					<table class="table table-dark table-striped">
						<thead>
							<tr class="text-center roboto-medium">
								<th>ID IDEA</th>
								<th>OBSERVACION TITULO</th>
								<th>OBSERVACION PALABARAS CLAVES</th>
								<th>OBSERVACION P.PROBLEMA</th>
								<th>OBSERVACION O.GENERARL</th>
								<th>OBSERVACION O.ESPECIFICO</th>
								<th>OBSERVACION JUSTIFICACION</th>
								<th>ESTADO</th>
                				<th>FECHA RETROALIMENTACION</th>

							</tr>
						</thead>

	<?php  
  
    if( isset($_POST['buscarOb']) )
    {

  
      $id_idea = $_POST['buscarOb'];


      $consulta ="SELECT id_proyecto_anterior,id_retroalimentacion_anterior,ob_titulo_anterior,ob_palabras_claves_anterior,ob_planteamiento_problema_anterior,
      ob_objetivo_general_anterior,ob_objetivo_especifico_anterior,ob_justificacion_anterior,estado_anterior,fecha_anterior
                  FROM retroalimentacion_idea_actualizados 
                  WHERE id_proyecto_anterior = $id_idea  ORDER BY fecha_anterior";
	         $datos = mysqli_query ($conexion,$consulta);

           while($raw = mysqli_fetch_assoc( $datos)){

             include("form-retroalimentacion-reporte.php");

           }
        
           
      mysqli_close($conexion);
    }
    ?>
</table>
</div>
      </div> <!-- cierre row-->
    </div> <!-- cierre container-->
  </section>
</main>


<script src="./js/sweetalert2.min.js" ></script>
    <script src="./js/jquery-3.5.1.min.js"></script>
    <script src="./js/enviar-datos.js"></script>
    <script src="./js/jquery-3.4.1.min.js" ></script>
	<script src="./js/popper.min.js" ></script>
	<script src="./js/bootstrap.min.js" ></script>
	<script src="./js/jquery.mCustomScrollbar.concat.min.js" ></script>
	<script src="./js/bootstrap-material-design.min.js" ></script>
	<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
	<script src="./js/main.js" ></script>
</body>
</html>