<?php
	session_start();

	if(!isset($_SESSION['rol'])){
		header('Location: index.php');
	}else{

		if($_SESSION['rol'] != 3){
			header('Location: index.php');
		}
	}

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Retroalimentacion</title>
	<script src="https://kit.fontawesome.com/9e00248cd3.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="./css/normalize.css">
	<link rel="stylesheet" href="./css/bootstrap.min.css">
	<link rel="stylesheet" href="./css/bootstrap-material-design.min.css">
	<link rel="stylesheet" href="./css/all.css">
	<link rel="stylesheet" href="./css/sweetalert2.min.css">
	<link rel="stylesheet" href="./css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="./css/animate.min.css">
	<link rel="stylesheet" href="./css/style.css">
	<link rel="stylesheet" href="./css/login.css">
</head>
<body>
	 <!-- Main container -->
	<main class="full-box main-container">
		<!-- Nav lateral -->
		<?php include ('nav-lateral-asesor.php') ?>
		<!-- Page content -->
		<section class="full-box page-content">
			<nav class="full-box navbar-info">
				<a href="#" class="float-left show-nav-lateral">
					<i class="fas fa-exchange-alt"></i>
				</a>
				<a href="user-update.php?usertype=3&userid=<?=$userid;?>&estadoid=<?=$idea_estado;?>">
					<i class="fas fa-user-cog"></i>
				</a>
				<a href="#" class="btn-exit-system">
					<i class="fas fa-power-off"></i>
				</a>
			</nav>

			<!-- Page header -->
			<div class="full-box page-header">
				<h3 class="text-left">
					<i class="fas fa-plus fa-fw"></i> &nbsp; Retroalimentacion
				</h3>
				<p class="text-justify text-dark">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem odit amet asperiores quis minus, dolorem repellendus optio doloremque error a omnis soluta quae magnam dignissimos, ipsam, temporibus sequi, commodi accusantium!
				</p>
			</div>

			<div class="container-fluid">
				<ul class="full-box list-unstyled page-nav-tabs">
					<li>
						<a class="active" href="registro_usuarios.php"><i class="fas fa-plus fa-fw"></i> &nbsp; RETROALIMETACION</a>
					</li>
					<li>
					    <a  href="asignar_asesor.php"><i class="fas fa-search fa-fw"></i> &nbsp; CONSULTA DE IDEAS</a>
                    </li>
                    <li>
						<a   href="registro_proyectos.php"><i class="fas fa-clipboard-list fa-fw"></i> &nbsp; COMO REDACTAR MI ANTEPRYECTO</a>
					</li>
				</ul>	
			</div>


			<div class="container-fluid"> 

			<form  class="form-neon" >

			<legend><i class="fas fa-user"></i> &nbsp; Poryectos Asignados Al Asesor</legend>
				<div class="container-fluid">

					<div class="row">

					<div class="col-12 col-md-12">
									<div class="table-responsive">
										<table class="table table-dark table-sm">
										<thead>
										<tr class="text-center roboto-medium">
										<th>ID_PROYECTOS</th>
										<th>ID_PROFESOR</th>
										</tr>
										</thead>
										<tbody id="myTable">
										<?php include 'search-asignacion.php'; ?>
										</tbody>						
										</table>
									</div>
								</div>
					</div>
				</div>

			</form>

			</div>

		<!-- Content here-->
		<div class="container-fluid">
    <form action="retroalimentacion-ideas.php?usertype=2&userid=<?=$userid;?>" method="post" autocomplete = "off">
      <div class="row">
	  
        <div class="col-12 col-md-6">
          <div class="form-group">
            <label for="cliente_apellido" class="bmd-label-floating">ID de la idea</label>
            <input type="number"  class="form-control" name="buscar" id="buscar" maxlength="6" required>
          </div>
        </div>
        <div class="col-12 col-md-6">
          <div class="form-group">
            <p style="margin-top: 30px;">
              <button type="submit" class="btn btn-raised btn-info" ><i class="fas fa-search"></i> &nbsp; Buscar</button>
            </p>
          </div>  
        </div>
      </div>
    </form>
	<?php include("create-retroalimentacion.php");?>
	<?php include("actualizar-retroalimentacion.php");?>

	<?php    
    if( isset($_POST['buscar']) )
    {
      $id_idea = $_POST['buscar'];
      $consulta ="SELECT *
                  FROM ideas 
                  WHERE id_proyecto = '$id_idea'";
	  $datos = mysqli_query ($conexion,$consulta);
	  
      $row  = mysqli_fetch_assoc($datos);

      if( $row <= 0)
      {
        echo "No se encontró información para la idea con ID: <b>" . $id_idea . "</b>";
      }else{
          include_once "retroalimentacion-form-principal.php";
      } 
      mysqli_close($conexion);
    }
    ?>
      </div> <!-- cierre row-->
    </div> <!-- cierre container-->
  </section>
</main>
	
	<script src="./js/sweetalert2.min.js" ></script>
    <script src="./js/jquery-3.5.1.min.js"></script>
    <script src="./js/enviar-datos.js"></script>
    <script src="./js/jquery-3.4.1.min.js" ></script>
	<script src="./js/popper.min.js" ></script>
	<script src="./js/bootstrap.min.js" ></script>
	<script src="./js/jquery.mCustomScrollbar.concat.min.js" ></script>
	<script src="./js/bootstrap-material-design.min.js" ></script>
	<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
	<script src="./js/main.js" ></script>
</body>
</html>