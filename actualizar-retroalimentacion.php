<?php
    include ('conexion.php');

    if (isset($_POST['Actualizar'])) {
        $nombre = $_FILES['documento']['name'];
        $ruta = $_FILES['documento']['tmp_name'];
        $destino = "documentos/" . $nombre;
        if ($nombre != "") {
            if (copy($ruta, $destino)) {
    
                $ob_titulo= $_POST["ob_titulo"];
                $ob_palabras_claves = $_POST["ob_palabras_claves"];
                $ob_problema = $_POST["ob_problema"];
                $ob_general = $_POST["ob_general"];
                $ob_especifico = $_POST["ob_especifico"];
                $job_justificacion = $_POST["ob_justificacion"];
                $documento = $nombre;
                $estado =$_POST['estado'];
                $id_proyecto = $_POST["id_proyecto"];
                $fecha =  date("Y/m/d");

               $consulta = "UPDATE retroalimentacion_idea SET ob_titulo='$ob_titulo',ob_palabras_claves='$ob_palabras_claves',
               ob_planteamiento_problema='$ob_problema',ob_objetivo_general='$ob_general',ob_objetivo_especifico='$ob_especifico',
               ob_justificacion='$job_justificacion',documento='$documento',estado='$estado',fecha='$fecha' WHERE id_proyecto = $id_proyecto";

                $resultados = mysqli_query($conexion,$consulta);

                if ($resultados) {
                    echo '<script>
					if(window.history.replaceState){
						window.history.replaceState(null,null,window.location.href);
					}
				</script>';

                    echo '<div class="alert alert-success text-center alert-dismissible fade show" role="alert">
                     LA ACTUALIZACION DE LA RETROALIMENTACION FUE EXITOSA
                  </div>';
                }
                else{
                    echo '<script>
					if(window.history.replaceState){
						window.history.replaceState(null,null,window.location.href);
					}
				</script>';

                    echo '<div class="alert alert-danger text-center alert-dismissible fade show" role="alert">
                     OHH HA OCURRIDO UN ERROR O ERROR DE CONEXION
                  </div>';
                }
            } 
            else {
                echo "Error";
            }
        }
    }
?>