<?php
	session_start();

	if(!isset($_SESSION['rol'])){
		header('Location: index.php');
	}else{

		if($_SESSION['rol'] != 1){
			header('Location: index.php');
		}
	}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro de Proyecto de Grado</title>
	<script src="https://kit.fontawesome.com/9e00248cd3.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="./css/normalize.css">
	<link rel="stylesheet" href="./css/bootstrap.min.css">
	<link rel="stylesheet" href="./css/bootstrap-material-design.min.css">
	<link rel="stylesheet" href="./css/all.css">
	<link rel="stylesheet" href="./css/sweetalert2.min.css">
	<link rel="stylesheet" href="./css/jquery.mCustomScrollbar.css">
</head>
<body>

	<link rel="stylesheet" href="./css/style.css">

        	<!-- Main container -->
	<main class="full-box main-container">
		<!-- Nav lateral -->
		<?php include ('nav-lateral-coordinador.php') ?>
		<!-- Page content -->
		<section class="full-box page-content">
			<nav class="full-box navbar-info">
				<a href="#" class="float-left show-nav-lateral">
					<i class="fas fa-exchange-alt"></i>
				</a>
				<a href="user-update.html">
					<i class="fas fa-user-cog"></i>
				</a>
				<a href="#" class="btn-exit-system">
					<i class="fas fa-power-off"></i>
				</a>
			</nav>

			<!-- Page header -->
			<div class="full-box page-header">
				<h3 class="text-left">
					<i class="fas fa-plus fa-fw"></i> &nbsp; Registro de Anteproyectos y Proyectos de grados
				</h3>
				<p class="text-justify">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem odit amet asperiores quis minus, dolorem repellendus optio doloremque error a omnis soluta quae magnam dignissimos, ipsam, temporibus sequi, commodi accusantium!
				</p>
			</div>

			
			
			<!-- Content here-->
			<div class="container-fluid">
				<form  id="Roproyecto" method="POST" class="form-neon" autocomplete="off" >
					<fieldset>
						<legend><i class="fas fa-user"></i> &nbsp; Información básica de las idea</legend>
						<div class="container-fluid">
                        <div class="row">

						<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="titulo" class="bmd-label-floating">Tíutlo</label>
								<textarea type="text"  class="form-control" name="titulo" id="titulo" required></textarea>
							</div>
						</div>

						<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="palabras_clave" class="bmd-label-floating">Palabras clave</label>
								<textarea type="text" class="form-control" name="palabras_clave" id="palabras_clave" required></textarea>
							</div>
						</div>

						<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="palabras_clave" class="bmd-label-floating">ID Estudiante:</label>
								<input type="number" class="form-control" name="id_usuario" id="cliente_dni" maxlength="27" required>
							</div>
						</div>

						<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="planteamiento_problema" class="bmd-label-floating">Nombres Estudiantes</label>
								<textarea type="text" class="form-control" name="name_estudiantes" id="planteamiento_problema" required></textarea>
							</div>
						</div>
						<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="objetivo_general" class="bmd-label-floating">Apellidos Estudiantes</label>
								<textarea type="text" class="form-control" name="Apellidos_Estudiantes"  required></textarea>
							</div>
						</div>

						<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="objetivos_especificos" class="bmd-label-floating">Telefono</label>
								<textarea type="text" class="form-control" name="Telefono" id="Telefono" required></textarea>
							</div>
						</div>

						<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="palabras_clave" class="bmd-label-floating">Correo Estudiante:</label>
								<input type="email" class="form-control" name="correo_estudiantes" id="cliente_dni" maxlength="27" required>
							</div>
						</div>

						<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="palabras_clave" class="bmd-label-floating">Nombres Asesor:</label>
								<input type="text" class="form-control" name="n_asesor" id="n_asesor" maxlength="27" required>
							</div>
						</div>

						<div class="col-12 col-md-6">
									<div class="form-group">
									<label for="usr">Estado</label>
                                    <select class="form-control" required id="estado" name="estado">
									<option value=""></option>
                                    <option value="Radicar Informe Final">Radicar Informe Final</option>
                                    <option value="Radicar Anteproyecto">Radicar Anteproyecto</option>
                                    </select>
									</div>
								</div>

							<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="objetivo_general" class="bmd-label-floating">Obervaciones</label>
								<textarea type="text" class="form-control" name="Obervaciones" id="objetivo_general" required></textarea>
							</div>
							</div>

							<div class="col-12 col-md-6">
									<div class="form-group">
									<label for="usr">Semestre</label>
                                    <select class="form-control" required id="semestre" name="semestre">
									<option value=""></option>
                                    <option value="Periodo de gracia">Periodo de gracia</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    </select>
									</div>
								</div>

							<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="objetivo_general" class="bmd-label-floating">Recomendacion</label>
								<textarea type="text" class="form-control" name="Recomendacion" id="objetivo_general" required></textarea>
							</div>
							</div>

							<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="objetivo_general" class="bmd-label-floating">Registro</label>
								<textarea type="text" class="form-control" name="Registro" id="objetivo_general" required></textarea>
							</div>
							</div>

							<div class="col-12 col-md-6">
									<div class="form-group">
									<label for="usr">Estado Proyecto</label>
                                    <select class="form-control" required id="Estado_Proyecto" name="Estado_Proyecto">
									<option value=""></option>
                                    <option value="Sin asignar asesor ">Sin asignar asesor </option>
                                    <option value="En revisión ">En revisión </option>
                                    <option value="Socializado">Socializado</option>
									<option value="Terminado">Terminado</option>
                                    </select>
									</div>
								</div>

					</fieldset>
					<br><br><br>
					<p class="text-center" style="margin-top: 40px;">
						<button type="reset" class="btn btn-raised btn-secondary btn-sm"><i class="fas fa-paint-roller"></i> &nbsp; LIMPIAR</button>
						&nbsp; &nbsp;
						<button id="RegistrarP"  class="btn btn-raised btn-info btn-sm"><i class="far fa-save"></i> &nbsp; GUARDAR</button>
					</p>
				</form>
			</div>

		</section>
	</main>

	<script src="./js/sweetalert2.min.js" ></script>
    <script src="./js/jquery-3.5.1.min.js"></script>
    <script src="./js/enviar-datos.js"></script>
    <script src="./js/jquery-3.4.1.min.js" ></script>
	<script src="./js/popper.min.js" ></script>
	<script src="./js/bootstrap.min.js" ></script>
	<script src="./js/jquery.mCustomScrollbar.concat.min.js" ></script>
	<script src="./js/bootstrap-material-design.min.js" ></script>
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
	<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
	<script src="./js/main.js" ></script>
</body>
</html>